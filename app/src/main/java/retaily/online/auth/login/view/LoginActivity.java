package retaily.online.auth.login.view;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.room.Room;

import retaily.online.BuildConfig;
import retaily.online.R;
import retaily.online.auth.pincode.view.PinCodeActivity;
import retaily.online.basicals.BaseActivity;
import retaily.online.database.AppDataBase;
import retaily.online.main.view.MainActivity;
import retaily.online.models.request.LoginBody;
import retaily.online.models.response.login.LoginBaseResponse;
import retaily.online.service.EventObserver;
import retaily.online.utils.AppConstants;
import retaily.online.auth.login.viewmodel.LoginViewModel;
import retaily.online.auth.registration.view.RegistrationActivity;
import retaily.online.utils.PrefKeys;

public class LoginActivity extends BaseActivity {

    private Button nextBtn;
    private Button regBtn;

    public static AppDataBase appDataBase;
    LoginViewModel loginViewModel;

    LinearLayout loginLayout;

    ConstraintLayout rootLayout;
    View loading;
    boolean isLoading = false;

    private boolean isFromForm = false;

    ActivityResultLauncher<Intent> activityResultLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

        rootLayout = findViewById(R.id.root_layout);
        loading = getLayoutInflater().inflate(R.layout.loading, rootLayout, false);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        loading.setLayoutParams(layoutParams);

        loginLayout.setVisibility(View.INVISIBLE);

        appDataBase = Room.databaseBuilder(getApplicationContext(), AppDataBase.class, AppConstants.DB_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
        loginViewModel = new ViewModelProvider(getViewModelStore(),
                new ViewModelProvider.AndroidViewModelFactory(getApplication()))
                .get(LoginViewModel.class);

        EditText login = findViewById(R.id.loginET);
        EditText pass = findViewById(R.id.passET);
        nextBtn.setOnClickListener(view -> {
            if (!login.getText().toString().isEmpty() && !pass.getText().toString().isEmpty()) {
                hideKeyboard();
                showLoginForm(false);
                isFromForm = true;
                tryLogin(login.getText().toString(), pass.getText().toString());
            }
        });
        regBtn.setOnClickListener(view -> {
            hideKeyboard();
            showRegPage();
        });

        checkAuthData();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rootLayout.getWindowToken(), 0);
    }

    public void init() {
        loginLayout = findViewById(R.id.login_layout);
        nextBtn = findViewById(R.id.nextBtn);
        regBtn = findViewById(R.id.regBtn);
        TextView versionName = findViewById(R.id.versionName);
        versionName.setText("ver " + BuildConfig.VERSION_NAME);

        activityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), result -> {
                if (result.getResultCode() == PinCodeActivity.RESULT_BACK_PRESSED) {
                    showLoginForm(true);
                } else if (result.getResultCode() == PinCodeActivity.RESULT_OK) {
                    String login = getSharedPreferences(AppConstants.PREF_NAME, MODE_PRIVATE).getString("login", "");
                    String pass = getSharedPreferences(AppConstants.PREF_NAME, MODE_PRIVATE).getString("pass", "");
                    tryLogin(login, pass);
                }
            });
    }

    private void checkAuthData() {
        String login = getSharedPreferences(AppConstants.PREF_NAME, MODE_PRIVATE).getString(PrefKeys.LOGIN_KEY, "");
        String pass = getSharedPreferences(AppConstants.PREF_NAME, MODE_PRIVATE).getString(PrefKeys.PASS_KEY, "");
        String user_id = getSharedPreferences(AppConstants.PREF_NAME, MODE_PRIVATE).getString(PrefKeys.CURRENT_USER_ID_KEY, "");
        if (login.equals("") || login.isEmpty() || pass.equals("") || pass.isEmpty() || user_id.equals("") || user_id.isEmpty()) {
            showLoginForm(true);
        } else {
            showPinPage();
        }
    }

    private void showLoginForm(boolean isVisible) {
        loginLayout.setVisibility(isVisible? View.VISIBLE : View.INVISIBLE);
    }

    private void tryLogin(String login, String pass) {
        if (!isLoading) {
            rootLayout.addView(loading);
            isLoading = true;
        }
        String model = Build.MODEL;
        int version = Build.VERSION.SDK_INT;

        loginViewModel.login(new LoginBody(login, pass, "Android", model + "/ver:" + version))
                .observe(this, new EventObserver<>(response -> {
                    LoginBaseResponse loginBaseResponse = response;
                    if (response.getError() == null) {
                        if (response.getResponse_code() == 200) {
                            getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE)
                                    .edit()
                                    .putString(PrefKeys.LOGIN_KEY, login)
                                    .putString(PrefKeys.PASS_KEY, pass)
                                    .putString(PrefKeys.TOKEN_KEY, response.getToken())
                                    .putString(PrefKeys.CURRENT_USER_ID_KEY, response.getUserInfo().code)
                                    .apply();

                            loginViewModel.insert(response.getUserInfo());

                            if (isFromForm) {
                                isFromForm = false;
                                if (isLoading) {
                                    rootLayout.removeView(loading);
                                    isLoading = false;
                                }
                                showPinPage();
                            } else {
                                getStatuses(response.getToken());
                            }
                        } else {
                            if (isLoading) {
                                rootLayout.removeView(loading);
                                isLoading = false;
                            }
                            handleError(response.getResponse_code(), response.getMessage(), false);
                            showLoginForm(true);
                        }
                    } else {
                        if (isLoading) {
                            rootLayout.removeView(loading);
                            isLoading = false;
                        }
                        Toast.makeText(this, LoginActivity.class.getName() + ": " + response.getError(), Toast.LENGTH_LONG).show();
                        showLoginForm(true);
                    }
                }));

    }

    private void getStatuses(String token) {
        showLoginForm(false);
        if (!isLoading) {
            rootLayout.addView(loading);
            isLoading = true;
        }
        loginViewModel.getStatuses(token).observe(this, new EventObserver<>(response -> {
            if (isLoading) {
                rootLayout.removeView(loading);
                isLoading = false;
            }
            if (response.getError() == null) {
                if(response.getResponse_code() == 200) {
                    loginViewModel.addStatuses(response.getStatuses()).observe(this, ids -> {});
                    showMainPage();
                } else {
                    handleError(response.getResponse_code(), response.getMessage(), true);
                }
            } else {
                Toast.makeText(this, LoginActivity.class.getName() + ": " + response.getError(), Toast.LENGTH_SHORT).show();
            }
        }));
    }

    private void showRegPage(){
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }

    private void showPinPage() {
        Intent intent = new Intent(this, PinCodeActivity.class);
        activityResultLauncher.launch(intent);
    }

    private void showMainPage() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}