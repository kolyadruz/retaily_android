package retaily.online.auth.login.viewmodel;

import android.app.Application;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;

import retaily.online.models.request.LoginBody;
import retaily.online.models.response.login.LoginBaseResponse;
import retaily.online.models.response.login.UserInfo;
import retaily.online.models.response.objects.status.Status;
import retaily.online.models.response.objects.status.StatusResponse;
import retaily.online.repositories.MainRepository;
import retaily.online.service.Event;
import retaily.online.service.EventObserver;
import retaily.online.utils.TrimmedTextWatcher;

import java.util.ArrayList;
import java.util.List;

public class LoginViewModel extends AndroidViewModel {

    LifecycleOwner lifecycleOwner;

    LoginBody loginBody;
    IloginAction iLoginAction;

    public LoginViewModel(@NonNull Application application) {
        super(application);
    }

    public TrimmedTextWatcher getLoginTextWatcher() {
        return new TrimmedTextWatcher() {
            @Override
            public void afterTextChanged(@NonNull final Editable editable) {
                loginBody.setLogin(editable.toString());
            }

            @Override
            public void onTextChanged(String newValue) {

            }
        };
    }

    public TrimmedTextWatcher getPasswordTextWatcher() {
        return new TrimmedTextWatcher() {
            @Override
            public void afterTextChanged(@NonNull final Editable editable) {
                loginBody.setPassword(editable.toString());
            }

            @Override
            public void onTextChanged(String newValue) {

            }
        };
    }

    public boolean onEditorAction(@NonNull final TextView textView, final int actionId,
                                  @Nullable final KeyEvent keyEvent) {
        //TextUtils.editorActionBaseCheck(textView, actionId, keyEvent) &&
        /*if (loginBody.isInputDataValid()) {
            requestSignIn();
        }*/
        return false;
    }

    public void onNextClick(@NonNull final View view) {
        //login();
    }

    public void onRegClick(@NonNull final View view) {
        iLoginAction.onRegClicked();
    }

    public void insert(UserInfo userInfo) {
        MainRepository.getInstance().insert(userInfo);
    }

    public LiveData<Event<LoginBaseResponse>> login(LoginBody loginBody) {
        return MainRepository.getInstance().login(loginBody);
    }

    public LiveData<Event<StatusResponse>> getStatuses(String token) {
        return MainRepository.getInstance().getStatuses(token);
    }

    public LiveData<List<Long>> addStatuses(ArrayList<Status> statuses) {
        return MainRepository.getInstance().addStatuses(statuses);
    }

}
