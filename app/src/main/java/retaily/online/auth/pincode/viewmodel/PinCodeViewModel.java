package retaily.online.auth.pincode.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import retaily.online.models.response.login.UserInfo;
import retaily.online.repositories.MainRepository;

public class PinCodeViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public PinCodeViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<UserInfo> getUserInfo(String code) {
        return MainRepository.getInstance().getUserInfo(code);
    }
}