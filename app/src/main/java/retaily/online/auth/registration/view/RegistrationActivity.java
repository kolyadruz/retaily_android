package retaily.online.auth.registration.view;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import retaily.online.R;

public class RegistrationActivity extends AppCompatActivity {

    private TextView mTextView;
    private Button callBtn;
    private ImageButton backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mTextView = (TextView) findViewById(R.id.text);
        callBtn = (Button) findViewById(R.id.callBtn);
        callBtn.setOnClickListener(view -> showCallDialog());
        backBtn = (ImageButton) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(view -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void showCallDialog() {

        String phones = "84112258889,84112318889";

        String[] separated = phones.split(",");

        AlertDialog.Builder phoneNumbersAlert = new AlertDialog.Builder(this);

        phoneNumbersAlert.setTitle("Выберите номер телефона для звонка");

        phoneNumbersAlert.setItems(separated, (
        DialogInterface dialog, int position) -> {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},1);
            } else {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel: " + separated[position]));
                startActivity(intent);
            }
        });

        phoneNumbersAlert.show();
    }
}