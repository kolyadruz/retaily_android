package retaily.online.basicals;

import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import retaily.online.utils.AppConstants;

public class BaseActivity extends AppCompatActivity {

    public void handleError(int responseCode, String message, boolean isClientV2Response) {
        switch (responseCode) {
            case 400:
                if (message != null) {
                    String firstSevenChars = message.substring(0, 6);
                    if (firstSevenChars.compareTo("client:") == 0) {
                        String msg = message.substring(7);
                        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                    } else {
                        if (!isClientV2Response) {
                            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Toast.makeText(this, AppConstants.ON_INTERNAL_SERVER_ERROR, Toast.LENGTH_SHORT).show();
                }
                break;
            case 401:
                Toast.makeText(this, AppConstants.ON_TOKEN_EXPIRED, Toast.LENGTH_SHORT).show();
                break;
            case 503:
                Toast.makeText(this, AppConstants.ON_SERVICE_UNAVAILABLE, Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }
}
