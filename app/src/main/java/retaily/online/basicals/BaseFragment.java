package retaily.online.basicals;

import android.content.Context;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import retaily.online.utils.AppConstants;

public class BaseFragment extends Fragment {

    public void handleError(Context context, int responseCode, String message, boolean isClientV2Response) {
        switch (responseCode) {
            case 400:
                if (message != null) {
                    String firstSevenChars = message.substring(0, 6);
                    if (firstSevenChars.compareTo("client:") == 0) {
                        String msg = message.substring(7);
                        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
                    } else if (!isClientV2Response) {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), AppConstants.ON_INTERNAL_SERVER_ERROR, Toast.LENGTH_SHORT).show();
                }
                break;
            case 401:
                Toast.makeText(getActivity(), AppConstants.ON_TOKEN_EXPIRED, Toast.LENGTH_SHORT).show();
                break;
            case 503:
                Toast.makeText(getActivity(), AppConstants.ON_SERVICE_UNAVAILABLE, Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    public void showComment(FragmentManager fragmentManager, String comment) {
        CommentDialog dialog = new CommentDialog(comment);
        dialog.show(fragmentManager, dialog.getTag());
    }

}
