package retaily.online.basicals;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import retaily.online.R;
import retaily.online.main.adapter.OnFragmentsActionListener;

public class CommentDialog extends DialogFragment implements View.OnClickListener {

    final String LOG_TAG = "myLogs";

    String message;

    public CommentDialog(String message) {
        this.message = message;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Изменения в товарах");
        View rootView = inflater.inflate(R.layout.dialog_order_added, null);
        rootView.findViewById(R.id.btnYes).setOnClickListener(this);
        ((TextView)rootView.findViewById(R.id.orderNum)).setText("");

        if (message!=null) {
            ((TextView) rootView.findViewById(R.id.message)).setText(message);
        }
        return rootView;
    }

    public void onClick(View v) {
        dismiss();
    }
}