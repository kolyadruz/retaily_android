package retaily.online.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import retaily.online.models.local.Cart;
import retaily.online.models.response.getcategories.Category;
import retaily.online.models.response.getproducts.Product;
import retaily.online.models.response.getshops.Shop;
import retaily.online.models.response.getsuppliers.Supplier;
import retaily.online.models.response.getsuppliers.Tag;
import retaily.online.models.response.login.UserInfo;
import retaily.online.models.response.objects.status.Status;
import retaily.online.utils.StringsArrayListConverter;
import retaily.online.utils.SuppliersListConverter;

@Database(entities = {UserInfo.class,
        Tag.class,
        Supplier.class,
        Product.class,
        Category.class,
        Shop.class,
        Cart.class,
        Status.class
        },
        version = 6,
        exportSchema = false)
@TypeConverters({SuppliersListConverter.class,
        StringsArrayListConverter.class
})

public abstract class AppDataBase extends RoomDatabase {

    public abstract MyDao myDao();

}