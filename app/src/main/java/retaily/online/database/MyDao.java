package retaily.online.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import retaily.online.models.local.Cart;
import retaily.online.models.local.CartHeader;
import retaily.online.models.request.ProductBody;
import retaily.online.models.response.getcategories.Category;
import retaily.online.models.response.getproducts.Product;
import retaily.online.models.response.getshops.Shop;
import retaily.online.models.response.getsuppliers.Supplier;
import retaily.online.models.response.getsuppliers.Tag;
import retaily.online.models.response.login.UserInfo;
import retaily.online.models.response.objects.status.Status;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface MyDao {

    /** UserInfo */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertuserinfo(UserInfo userInfo);
    @Query("select * from userinfo where user_code = :code")
    LiveData<UserInfo> getuserinfo(String code);
    @Query("select * from userinfo")
    UserInfo getuserinfosimple();
    @Delete
    void deleteuserinfo(UserInfo userInfo);

    /** Tag */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void inserttags(ArrayList<Tag> tags);
    @Query("select * from suppliers_tags")
    LiveData<Tag> gettags();
    @Delete
    void deletetags(ArrayList<Tag> tags);

    /** Supplier */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insertsuppliers(ArrayList<Supplier> suppliers);
    @Query("select * from suppliers where supplier_code = :code")
    Supplier getsupplier(String code);
    @Delete
    void deletesupplier(ArrayList<Supplier> suppliers);

    /** Category */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insertcategories(ArrayList<Category> categories);
    @Query("select * from categories where category_code = :code")
    Category getcategory(String code);
    @Delete
    void deletecategory(Category category);

    /** Product */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertproduct(Product product);
    @Query("select * from products")
    LiveData<Product> getproduct();
    @Delete
    void deleteproduct(Product product);

    /** Shops */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertshop(Shop shop);
    @Query("select * from shops")
    LiveData<Shop> getshops();
    @Delete
    void deleteshop(Shop shop);

    /** Cart */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addToCart(Cart cart);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> addToCart(List<Cart> carts);

    @Query("select cart_quantity from cart where product_supplier = :supplierCode and product_code = :productCode")
    long checkInCart(String supplierCode, String productCode);

    @Query("select cart_quantity from cart where product_supplier = :supplierCode and product_code = :productCode")
    LiveData<Long> checkDetailInCart(String supplierCode, String productCode);

    @Query("select sum(cart_quantity * product_price) from cart")
    LiveData<Double> getAllProductsSumInCart();

    @Query("select * from cart where product_supplier = :supplierCode and product_code = :productCode")
    Cart getCart(String supplierCode, String productCode);

    @Query("select sum(cart_quantity * product_price) as total_cost, product_supplier, product_balance from cart group by product_supplier")
    LiveData<List<CartHeader>> getCartHeaders();

    @Query("select * from cart where product_supplier = :supplierCode")
    List<Cart> getProductsBySupplier(String supplierCode);

    @Query("select product_supplier from cart group by product_supplier")
    List<String> getSuppliersInCart();

    @Query("select product_code as product, cart_quantity as quantity from cart where product_supplier = :supplierCode")
    List<ProductBody> getProductCodesBySupplier(String supplierCode);

    @Delete
    void deleteCart(Cart cart);

    @Query("delete from cart where product_supplier = :supplierCode")
    void deleteCart(String supplierCode);

    @Query("delete from cart")
    void clearAllFromCart();

    /** Statuses */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insertStatuses(List<Status> statuses);

    @Query("select * from statuses")
    LiveData<List<Status>> getStatuses();

}