package retaily.online.main.adapter;

import android.content.Intent;

import retaily.online.main.tabs.myorders.orders.adapter.OnOrderItemClick;
import retaily.online.main.tabs.cart.shops.adapter.OnConfirmClickListener;
import retaily.online.main.tabs.home.products.adapter.OnDetailItemClickListener;
import retaily.online.models.response.getbanners.Banner;
import retaily.online.models.response.getproducts.Product;
import retaily.online.models.response.getshops.Shop;

public interface OnFragmentsActionListener {

    void showLoginPage(boolean isTokenExpired);

    void showProductDetail(OnDetailItemClickListener listener, Product product, String supplierName, int itemPosition, boolean hasContract);
    void showBannerDetail(Banner banner);
    void showConfirmDialog(OnConfirmClickListener listener, Shop shop, double summ, String supplierCode);

    void showOrderAddedDialog(String orderCode, String message);

    void changeCartIcon(boolean isEmpty);

    void setTab(int position);

    void startIntent(Intent intent);

    void showDeleteDialog(int orderNum, String orderCode, long orderBalance, OnOrderItemClick listener);

    void showCallSupplierForContractDialog(String phones, boolean isAboutContract);
}
