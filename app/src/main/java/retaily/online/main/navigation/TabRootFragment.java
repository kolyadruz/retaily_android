package retaily.online.main.navigation;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import retaily.online.R;

import org.jetbrains.annotations.NotNull;

public class TabRootFragment extends Fragment {

    private static final String KEY_LAYOUT = "layout_key";
    private static final String KEY_TOOLBAR = "toolbar_key";
    private static final String KEY_NAV_HOST = "nav_host_key";

    private final int defaultInt = -1;
    private int layoutRes = -1;
    private int toolbarId = -1;
    private int navHostId = -1;
    private final AppBarConfiguration appBarConfig = new AppBarConfiguration.Builder(
                                                            R.id.suppliers_dest,
                                                            R.id.cart_dest,
                                                            R.id.orders_dest,
                                                            R.id.settings_dest
                                                        ).build();

    public static TabRootFragment newInstance(int layoutRes, int toolbarId, int navHostId) {
        Bundle args = new Bundle();
        args.putInt(KEY_LAYOUT, layoutRes);
        args.putInt(KEY_TOOLBAR, toolbarId);
        args.putInt(KEY_NAV_HOST, navHostId);

        TabRootFragment fragment = new TabRootFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            layoutRes = getArguments().getInt(KEY_LAYOUT);
            toolbarId = getArguments().getInt(KEY_TOOLBAR);
            navHostId = getArguments().getInt(KEY_NAV_HOST);
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(layoutRes != defaultInt) {
            return inflater.inflate(layoutRes, container, false);
        }
        return null;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getActivity() != null && toolbarId != defaultInt && navHostId != defaultInt) {
            Toolbar toolbar = requireActivity().findViewById(toolbarId);
            NavController navController = Navigation.findNavController(requireActivity(), navHostId);
            NavigationUI.setupWithNavController(toolbar, navController, appBarConfig);
        }
    }

    public boolean onBackPressed() {
        NavController navController = Navigation.findNavController(requireActivity(), navHostId);
        return navController.navigateUp();
    }

    public void popToRoot() {
        NavController navController = Navigation.findNavController(requireActivity(), navHostId);
        navController.popBackStack(navController.getGraph().getStartDestination(), false);
    }

    public boolean handleDeepLink(Intent intent) {
        NavController navController = Navigation.findNavController(requireActivity(), navHostId);
        return navController.handleDeepLink(intent);
    }
}
