package retaily.online.main.tabs.cart.cart.adater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import retaily.online.R;
import retaily.online.models.local.Cart;
import retaily.online.models.local.CartFooter;
import retaily.online.models.local.CartHeader;
import retaily.online.models.response.TypedCartItem;
import retaily.online.models.response.getproducts.Product;
import retaily.online.utils.AppConstants;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    OnCartItemClickListener listener;
    ArrayList<TypedCartItem> productsWithTitles;

    public CartAdapter(ArrayList<TypedCartItem> productsWithTitles, OnCartItemClickListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        this.productsWithTitles = productsWithTitles;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_category,parent,false);
                return new CartAdapter.CategoryHolder(view);
            case 1:
                View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_product,parent,false);
                return new CartAdapter.ProductHolder(view2);
            case 2:
                View view3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_button,parent,false);
                return new CartAdapter.ButtonHolder(view3);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case 0:
                CartAdapter.CategoryHolder categoryHolder = (CartAdapter.CategoryHolder) holder;
                CartHeader cartHeader = ((CartHeader) productsWithTitles.get(position));

                categoryHolder.productCategoryTitle.setText(cartHeader.getSupplierName());
                break;
            case 1:
                CartAdapter.ProductHolder productHolder = (CartAdapter.ProductHolder) holder;
                Cart cart = (Cart)productsWithTitles.get(position);

                Product product = cart.getProduct();
                ArrayList<String> images = product.getImages();

                if (images.size() > 0) {
                    Glide.with(context)
                            .load(AppConstants.SERVICE_URL + AppConstants.REPO + images.get(0))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(productHolder.productImage);
                } else {
                    productHolder.productImage.setImageResource(R.mipmap.supplier_holder);
                }

                productHolder.productName.setText(product.getName());
                productHolder.productDescShort.setText(product.getDescription_short());

                productHolder.quantity = productHolder.checkInCart(product);

                long quantum = product.getQuantum();
                long step = product.getStep();

                productHolder.minusBtn.setOnClickListener(view -> {
                    if (productHolder.quantity > quantum) {
                        productHolder.addToCart(product, productHolder.quantity - step);
                    } else {
                        productHolder.deleteFromCart(product);
                    }
                });

                productHolder.plusBtn.setOnClickListener(view -> {
                    if (product.getBalance() != 0 && productHolder.quantity < product.getBalance()) {
                        productHolder.addToCart(product, productHolder.quantity + step);
                    } else {
                        if (productHolder.quantity < quantum) {
                            productHolder.addToCart(product, productHolder.quantity + quantum);
                        } else {
                            productHolder.addToCart(product, productHolder.quantity + step);
                        }
                    }
                });

                productHolder.quantityTV.setText(""+ cart.getQuantity());
                productHolder.productPriceTV.setVisibility(product.getBalance() == 0 ? View.VISIBLE : View.GONE);
                productHolder.productPriceTV.setText(product.getPrice() + " ₽");

                productHolder.setOnClickListener(product.getCode());
                break;
            case 2:
                CartAdapter.ButtonHolder buttonHolder = (CartAdapter.ButtonHolder) holder;
                CartFooter cartFooter = ((CartFooter) productsWithTitles.get(position));

                if (cartFooter.getBalance() == 0) {
                    buttonHolder.checkOutBtn.setText("Оформить " + String.format("%.2f", cartFooter.getTotal_cost()) + " ₽");
                } else {
                    buttonHolder.checkOutBtn.setText("Оформить");
                }
                buttonHolder.checkOutBtn.setOnClickListener(view ->
                        listener.checkOutProducts(cartFooter.getSupplierCode(), cartFooter.getBalance())
                );
                break;
            default:
                break;
        }

    }

    @Override
    public int getItemCount() {
        return productsWithTitles.size();
    }

    @Override
    public int getItemViewType(int position) {
        return productsWithTitles.get(position).getType();
    }

    public class CategoryHolder extends RecyclerView.ViewHolder {

        TextView productCategoryTitle;
        //TextView productSumm;

        //Button checkOutBtn;

        public CategoryHolder(@NonNull View itemView) {
            super(itemView);
            productCategoryTitle = itemView.findViewById(R.id.cart_product_supplier);
            //productSumm = itemView.findViewById(R.id.cart_product_summ);
            //checkOutBtn = itemView.findViewById(R.id.checkout_btn);
        }
    }

    public class ProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView productImage;
        TextView productName;
        TextView productDescShort;
        TextView productPriceTV;

        ImageButton minusBtn;
        ImageButton plusBtn;
        TextView quantityTV;

        public long quantity;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.product_image);
            productName = itemView.findViewById(R.id.product_name);
            productDescShort = itemView.findViewById(R.id.product_desc_short);
            productPriceTV = itemView.findViewById(R.id.product_price);

            plusBtn = itemView.findViewById(R.id.plus_btn);
            minusBtn = itemView.findViewById(R.id.minus_btn);
            quantityTV = itemView.findViewById(R.id.quantityTV);

        }

        private void setOnClickListener(String productCode) {
            itemView.setTag(productCode);
            itemView.setOnClickListener(this);
        }

        private long checkInCart(Product product) {
            return listener.checkAmountInCart(product);
        }

        private long addToCart(Product product, long quantity) {
            return listener.addToCart(product, quantity);
        }

        private void deleteFromCart(Product product) {
            listener.deleteFromCart(product);
        }

        @Override
        public void onClick(View v) {
            //listener.onProductClick((String) v.getTag());
        }
    }

    public class ButtonHolder extends RecyclerView.ViewHolder {

        //TextView productCategoryTitle;
        //TextView productSumm;

        Button checkOutBtn;

        public ButtonHolder(@NonNull View itemView) {
            super(itemView);
            //productCategoryTitle = itemView.findViewById(R.id.cart_product_supplier);
            //productSumm = itemView.findViewById(R.id.cart_product_summ);
            checkOutBtn = itemView.findViewById(R.id.checkout_btn);
        }
    }

    public Context getContext() {
        return context;
    }

    public void deleteFromCart(int position) {
        TypedCartItem itemToDelete = productsWithTitles.get(position);
        if (itemToDelete.getType() == 0) {
            listener.deleteSupplierFromCart(((CartHeader)itemToDelete).getProduct_supplier());
        } else {
            listener.deleteFromCart(((Cart)itemToDelete).getProduct());
        }
    }

    public void updateProducts(ArrayList<TypedCartItem> productsWithTitles) {
        this.productsWithTitles = productsWithTitles;
        notifyDataSetChanged();
    }
}
