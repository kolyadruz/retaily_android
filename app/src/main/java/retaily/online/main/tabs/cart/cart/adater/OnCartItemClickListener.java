package retaily.online.main.tabs.cart.cart.adater;

import retaily.online.models.response.getproducts.Product;

public interface OnCartItemClickListener {

    long addToCart(Product product, long quantity);

    long checkAmountInCart(Product product);

    void deleteFromCart(Product product);

    void deleteSupplierFromCart(String supplierCode);

    void checkOutProducts(String supplierCode, long balance);

}
