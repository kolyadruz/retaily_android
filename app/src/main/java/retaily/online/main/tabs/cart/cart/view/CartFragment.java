package retaily.online.main.tabs.cart.cart.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.loader.content.AsyncTaskLoader;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.AsyncNotedAppOp;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import retaily.online.R;
import retaily.online.basicals.BaseFragment;
import retaily.online.main.tabs.cart.cart.adater.CartAdapter;
import retaily.online.main.tabs.cart.cart.adater.SwipeToDeleteCallback;
import retaily.online.main.tabs.cart.cart.adater.OnCartItemClickListener;
import retaily.online.main.adapter.OnFragmentsActionListener;
import retaily.online.main.tabs.cart.cart.viewmodel.CartViewModel;
import retaily.online.main.tabs.cart.shops.adapter.OnConfirmClickListener;
import retaily.online.main.tabs.cart.shops.view.ShopsFragment;
import retaily.online.models.local.Cart;
import retaily.online.models.local.CartFooter;
import retaily.online.models.local.CartHeader;
import retaily.online.models.request.CmdBody;
import retaily.online.models.request.ProductBody;
import retaily.online.models.response.TypedCartItem;
import retaily.online.models.response.checkproducts.Comment;
import retaily.online.models.response.getproducts.Product;
import retaily.online.models.response.getsuppliers.Supplier;
import retaily.online.service.EventObserver;
import retaily.online.utils.AppConstants;
import retaily.online.utils.PrefKeys;

import java.util.ArrayList;
import java.util.List;

public class CartFragment extends BaseFragment implements OnCartItemClickListener, OnConfirmClickListener {

    Context context;
    OnFragmentsActionListener listener;
    CartViewModel cartViewModel;

    String token;

    RecyclerView cartList;
    ArrayList<TypedCartItem> typedCartItems = new ArrayList<>();
    CartAdapter cartAdapter;

    LinearLayout emptyCartLayout;
    Button goToMainBtn;

    ConstraintLayout rootLayout;
    View loading;
    boolean isLoading = false;

    Toolbar toolbar;

    public CartFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        listener = (OnFragmentsActionListener)context;
        token = context.getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE).getString(PrefKeys.TOKEN_KEY, "");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cart, container, false);

        rootLayout = rootView.findViewById(R.id.root_layout);

        loading = inflater.inflate(R.layout.loading, container, false);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        loading.setLayoutParams(layoutParams);

        cartList = rootView.findViewById(R.id.cartList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        cartList.setLayoutManager(linearLayoutManager);
        cartAdapter = new CartAdapter(typedCartItems, this, context);
        cartList.setAdapter(cartAdapter);

        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback(cartAdapter));
        itemTouchHelper.attachToRecyclerView(cartList);

        emptyCartLayout = rootView.findViewById(R.id.empty_cart_layout);
        goToMainBtn = rootView.findViewById(R.id.go_main_button);
        goToMainBtn.setOnClickListener(view -> listener.setTab(0));

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cartViewModel = ViewModelProviders.of(requireActivity()).get(CartViewModel.class);
        cartViewModel.getCartHeaders().observe(getViewLifecycleOwner(), cartHeaders -> {
            typedCartItems.clear();
            if (cartHeaders != null && cartHeaders.size() > 0) {
                hideEmpty();
                for (CartHeader cartHeader: cartHeaders) {
                    Supplier supplier = cartViewModel.getSupplier(cartHeader.getProduct_supplier());
                    cartHeader.setSupplierName(supplier.getName());
                    typedCartItems.add(cartHeader);

                    List<Cart> cartsToAdd = cartViewModel.getCartsBySupplier(cartHeader.getProduct_supplier());
                    if (cartsToAdd != null && cartsToAdd.size() > 0) {
                        typedCartItems.addAll(cartsToAdd);
                    } else {
                        cartViewModel.deleteSupplierFromCart(cartHeader.getProduct_supplier());
                    }

                    CartFooter cartButton = new CartFooter(cartHeader.getProduct_supplier(), cartHeader.getTotal_cost(), cartHeader.getProduct_balance());
                    typedCartItems.add(cartButton);
                }
            } else {
                showEmpty();
            }
            updateList();
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        toolbar = requireActivity().findViewById(R.id.toolbar_cart);
        if (toolbar.getMenu() != null) {
            toolbar.getMenu().clear();
        }
        toolbar.inflateMenu(R.menu.cart_menu);
        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.action_delete_all && typedCartItems.size() > 0) {
                showConfirmCancelAllDialog();
            }
            return false;
        });

        List<String> supplierCodes = cartViewModel.getSuppliersInCart();
        for (String supplierCode : supplierCodes) {
            CmdBody cmdBody = new CmdBody("checkProducts");
            cmdBody.setSupplier(supplierCode);
            List<ProductBody> productBodies = cartViewModel.getProductCodesBySupplier(supplierCode);
            cmdBody.setProducts(productBodies);

            if (!isLoading) {
                rootLayout.addView(loading);
                isLoading = true;
            }

            cartViewModel.checkProducts(token, cmdBody).observe(getViewLifecycleOwner(), new EventObserver<>(checkProductsResponse -> {
                if (checkProductsResponse.getError() == null) {
                    if (checkProductsResponse.getResponse_code() == 200) {
                        List<Product> products = checkProductsResponse.getProducts();
                        List<Comment> comments = checkProductsResponse.getComments();

                        ArrayList<Cart> cartsToAdd = new ArrayList<>();

                        if (products.size() > 0) {
                            List<Cart> carts = cartViewModel.getCartsBySupplier(supplierCode);
                            for (Cart cart : carts) {
                                for (Product product : products) {
                                    if (cart.getProduct().getCode().equals(product.getCode())) {
                                        cart.setProduct(product);
                                        cartsToAdd.add(cart);
                                        break;
                                    }
                                }
                            }

                            if (comments.size() > 0) {
                                StringBuilder fullComment = new StringBuilder();
                                for(Comment comment: comments) {
                                    Product product = comment.getProduct();
                                    String message = comment.getComment();

                                    fullComment.append(product.getName()).append(" - ").append(message).append("/n");
                                }
                                if ((fullComment.length() > 0) && !fullComment.toString().equals("")) {
                                    showComment(getChildFragmentManager(), fullComment.toString());
                                }
                            }

                            cartViewModel.deleteSupplierFromCart(supplierCode);
                            cartViewModel.addToCart(cartsToAdd);

                            if (isLoading) {
                                rootLayout.removeView(loading);
                                isLoading = false;
                            }

                        } else {
                            listener.setTab(0);
                        }
                    } else {
                        listener.setTab(0);
                    }
                }
            }));
        }
    }

    private void showConfirmCancelAllDialog() {

        AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
        adb.setTitle("Вы уверены что хотите очистить заявки?");
        adb.setPositiveButton("Да", (dialog, which) -> cartViewModel.clearAllFromCart());
        adb.setNegativeButton("Нет", (dialog, which) -> {
        });

        AlertDialog dialog = adb.create();

        dialog.show();

        Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(getResources().getColor(R.color.btn_blue));
        Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(getResources().getColor(R.color.btn_blue));

    }

    private void updateList() {
        cartAdapter.updateProducts(typedCartItems);
    }

    private void showEmpty(){
        listener.changeCartIcon(true);
        emptyCartLayout.setVisibility(View.VISIBLE);
    }

    private void hideEmpty(){
        listener.changeCartIcon(false);
        emptyCartLayout.setVisibility(View.GONE);
    }

    @Override
    public long addToCart(Product product, long quantity) {
        Cart cart = cartViewModel.getCart(product);
        if (cart == null) {
            cart = new Cart(product);
        }
        cart.setQuantity(quantity);
        return cartViewModel.addToCart(cart);
    }

    @Override
    public long checkAmountInCart(Product product) {
        return cartViewModel.checkAmountInCart(product);
    }

    @Override
    public void deleteFromCart(Product product) {
        Cart cart = cartViewModel.getCart(product);
        cartViewModel.deleteFromCart(cart);
    }

    @Override
    public void deleteSupplierFromCart(String supplierCode) {
        cartViewModel.deleteSupplierFromCart(supplierCode);
    }

    @Override
    public void checkOutProducts(String supplierCode, long balance) {
        List<Cart> supplierProducts = cartViewModel.getCartsBySupplier(supplierCode);
        Supplier supplier = cartViewModel.getSupplier(supplierCode);

        Gson gson = new Gson();
        String jsonProducts = gson.toJson(supplierProducts);

        if (balance == 0) {
            NavDirections direction = CartFragmentDirections.actionNavCartToNavShops(jsonProducts, supplier.getName(), supplierCode);
            Navigation.findNavController(requireActivity(), R.id.nav_host_cart).navigate(direction);
        } else {
            long total_quantity = 0;
            for(Cart cart: supplierProducts) {
                total_quantity += cart.getQuantity();
            }
            listener.showConfirmDialog(this, null, total_quantity, supplierCode);
        }
    }

    @Override
    public void onConfirm(String supplierCode) {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    String fireBaseToken = "";

                    if (task.isSuccessful() && task.getResult() != null) {
                        Log.d("Installations", "Installation fb token: " + task.getResult());
                        fireBaseToken = task.getResult();
                    } else {
                        //Log.e("Installations", "Unable to get Installation auth token");
                        if (getActivity() != null) {
                            Toast.makeText(getActivity().getApplicationContext(), "Не удалось получить токен FCM", Toast.LENGTH_LONG).show();
                        }
                    }

                    CmdBody cmdBody = new CmdBody("setOrders");
                    cmdBody.setFBtoken(fireBaseToken);
                    setOrdersBalance(supplierCode, cmdBody);

                });
    }

    private void setOrdersBalance(String supplierCode, CmdBody cmdBody) {

        List<Cart> supplierProducts = cartViewModel.getCartsBySupplier(supplierCode);

        ArrayList<ProductBody> productsBody = new ArrayList<>();
        for (Cart cart: supplierProducts) {
            Product product = cart.getProduct();
            productsBody.add(new ProductBody(product.getCode(), cart.getQuantity()));
        }

        cmdBody.setPayment("1");
        cmdBody.setDelivery("1");
        cmdBody.setProducts(productsBody);
        cmdBody.setSupplier(supplierProducts.get(0).getProduct().getSupplier());

        cartViewModel.setOrdersBalance(token, cmdBody).observe(getViewLifecycleOwner(), new EventObserver<>(response -> {
            if (response.getError() == null) {
                if (response.getResponse_code() == 200) {
                    cartViewModel.deleteSupplierFromCart(supplierCode);
                    listener.showOrderAddedDialog(""+response.getOrder().getNum(), response.getOrder().getMessage());
                } else {
                    handleError(context, response.getResponse_code(), response.getMessage(), true);
                }
            } else {
                Toast.makeText(context, ShopsFragment.class.getName() + ": " + response.getError(), Toast.LENGTH_SHORT).show();
            }
        }));
    }

    class ClearDiskCache extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Glide.get(context).clearDiskCache();
            return null;
        }
    }

}