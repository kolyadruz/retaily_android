package retaily.online.main.tabs.cart.cart.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import retaily.online.models.local.Cart;
import retaily.online.models.local.CartHeader;
import retaily.online.models.request.CmdBody;
import retaily.online.models.request.ProductBody;
import retaily.online.models.response.checkproducts.CheckProductsResponse;
import retaily.online.models.response.getproducts.Product;
import retaily.online.models.response.getproducts.ProductResponse;
import retaily.online.models.response.getsuppliers.Supplier;
import retaily.online.models.response.setorders.SetOrdersResponse;
import retaily.online.repositories.MainRepository;
import retaily.online.service.Event;

import java.util.ArrayList;
import java.util.List;

public class CartViewModel extends AndroidViewModel {
    public CartViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<CartHeader>> getCartHeaders() {
        return MainRepository.getInstance().getCartHeaders();
    }

    public List<Cart> getCartsBySupplier(String supplierCode) {
        return MainRepository.getInstance().getCartsBySupplier(supplierCode);
    }

    public LiveData<Event<ProductResponse>> getProducts(String token, CmdBody cmdBody) {
        return MainRepository.getInstance().getProducts(token, cmdBody);
    }

    public Supplier getSupplier(String supplierCode) {
        return MainRepository.getInstance().getSupplier(supplierCode);
    }

    public List<String> getSuppliersInCart() {
        return MainRepository.getInstance().getSuppliersInCart();
    }

    public List<ProductBody> getProductCodesBySupplier(String supplierCode) {
        return MainRepository.getInstance().getProductCodesBySupplier(supplierCode);
    }

    public long addToCart(Cart cart) {
        return MainRepository.getInstance().addToCart(cart);
    }

    public LiveData<List<Long>> addToCart(ArrayList<Cart> carts) {
        return MainRepository.getInstance().addToCart(carts);
    }

    public void deleteFromCart(Cart cart) {
        MainRepository.getInstance().deleteFromCart(cart);
    }

    public void clearAllFromCart() {
        MainRepository.getInstance().clearAllFromCart();
    }

    public void deleteSupplierFromCart(String supplierCode) {
        MainRepository.getInstance().deleteSupplierFromCart(supplierCode);
    }

    public long checkAmountInCart(Product product) {
        return MainRepository.getInstance().checkInCart(product);
    }

    public Cart getCart(Product product) {
        return MainRepository.getInstance().getCart(product);
    }

    public LiveData<Double> getSumInCart() {
        return MainRepository.getInstance().getAllProductsSumInCart();
    }

    public LiveData<Event<SetOrdersResponse>> setOrdersBalance(String token, CmdBody cmdBody) {
        return MainRepository.getInstance().setOrdersBalance(token, cmdBody);
    }

    public LiveData<Event<CheckProductsResponse>> checkProducts(String token, CmdBody cmdBody) {
        return MainRepository.getInstance().checkProducts(token, cmdBody);
    }
}

