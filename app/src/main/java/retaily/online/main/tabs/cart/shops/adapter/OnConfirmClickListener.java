package retaily.online.main.tabs.cart.shops.adapter;

public interface OnConfirmClickListener {
    void onConfirm(String supplierCode);
}
