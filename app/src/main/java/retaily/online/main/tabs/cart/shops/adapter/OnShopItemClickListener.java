package retaily.online.main.tabs.cart.shops.adapter;

public interface OnShopItemClickListener {

    void onClick(String shopCode);

}
