package retaily.online.main.tabs.cart.shops.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import retaily.online.R;
import retaily.online.models.response.getshops.Shop;

import java.util.ArrayList;

public class ShopsAdapter extends RecyclerView.Adapter<ShopsAdapter.ViewHolder> {

    ArrayList<Shop> shops;
    Context context;
    OnShopItemClickListener listener;

    public ShopsAdapter(ArrayList<Shop> shops, OnShopItemClickListener listener, Context context) {
        this.listener = listener;
        this.shops = shops;
        this.context = context;
    }

    @NonNull
    @Override
    public ShopsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shop, parent, false);
        return new ShopsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopsAdapter.ViewHolder holder, int position) {
        holder.shopName.setText(shops.get(position).getName());

        Shop shop = shops.get(position);

        holder.shopAddress.setVisibility(shop.getStreet() == null || shop.getStreet().equals("")? View.GONE : View.VISIBLE);
        holder.shopAddress.setText(shop.getStreet() + ", " + shop.getHouse());

        if (listener != null) {
            holder.setOnClickListener(shops.get(position).getCode());
        }
    }

    @Override
    public int getItemCount() {
        return shops.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView shopName;
        TextView shopAddress;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            shopName = itemView.findViewById(R.id.shop_name);
            shopAddress = itemView.findViewById(R.id.shop_address);
        }

        private void setOnClickListener(String code) {
            itemView.setTag(code);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick((String) v.getTag());
        }

    }

    public void updateShops(ArrayList<Shop> shops) {
        this.shops = shops;
        notifyDataSetChanged();
    }
}