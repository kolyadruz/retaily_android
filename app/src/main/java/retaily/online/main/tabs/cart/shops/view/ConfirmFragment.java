package retaily.online.main.tabs.cart.shops.view;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;

import retaily.online.R;
import retaily.online.main.tabs.cart.shops.adapter.OnConfirmClickListener;
import retaily.online.models.response.getshops.Shop;

public class ConfirmFragment extends BottomSheetDialogFragment {

    private OnConfirmClickListener listener;

    TextView shopName;
    TextView shopAddress;
    TextView orderSumm;

    Button confirmBtn;

    ImageButton closeBtn;

    Shop shop;
    double summ;
    String supplierCode;

    private static final String KEY_SHOP = "shop_key";
    private static final String KEY_SUMM = "summ_key";
    private static final String KEY_SUPPLIER_CODE = "supplier_code_key";

    public ConfirmFragment(){}

    public static ConfirmFragment newInstance(String shop, double summ, String supplierCode) {
        Bundle args = new Bundle();

        args.putString(KEY_SHOP, shop);
        args.putDouble(KEY_SUMM, summ);
        args.putString(KEY_SUPPLIER_CODE, supplierCode);

        ConfirmFragment fragment = new ConfirmFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (OnConfirmClickListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            shop = new Gson().fromJson(getArguments().getString(KEY_SHOP), Shop.class);
            summ = getArguments().getDouble(KEY_SUMM);
            supplierCode = getArguments().getString(KEY_SUPPLIER_CODE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.bottom_sheet_order_confirm, container, false);

        closeBtn = rootView.findViewById(R.id.close_button);
        closeBtn.setOnClickListener(view -> {
            dismissSelf();
        });

        shopName = rootView.findViewById(R.id.shop_name);
        shopAddress = rootView.findViewById(R.id.shop_address);
        orderSumm = rootView.findViewById(R.id.cart_sum);

        if (shop != null) {
            shopName.setText(shop.getName());
            shopAddress.setVisibility(shop.getStreet() == null || shop.getStreet().equals("") ? View.GONE : View.VISIBLE);
            shopAddress.setText(shop.getStreet() + ", " + shop.getHouse());
            orderSumm.setText(String.format("%.2f", summ) + " ₽");
        } else {
            shopName.setVisibility(View.GONE);
            shopAddress.setVisibility(View.GONE);
            orderSumm.setText((int)summ + " шт");
        }

        confirmBtn = rootView.findViewById(R.id.confirm_button);
        confirmBtn.setOnClickListener(view -> {
            listener.onConfirm(supplierCode);
            dismiss();
        });

        return rootView;
    }

    private void dismissSelf() {
        this.dismiss();
    }
}
