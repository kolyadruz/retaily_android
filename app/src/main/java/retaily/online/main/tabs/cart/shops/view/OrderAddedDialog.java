package retaily.online.main.tabs.cart.shops.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import retaily.online.R;
import retaily.online.main.adapter.OnFragmentsActionListener;

public class OrderAddedDialog extends DialogFragment implements View.OnClickListener {

    final String LOG_TAG = "myLogs";

    String order_num;
    String message;

    OnFragmentsActionListener listener;

    public OrderAddedDialog(String order_num, OnFragmentsActionListener listener, String message) {
        this.order_num = order_num;
        this.listener = listener;
        this.message = message;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Успешно!");
        View rootView = inflater.inflate(R.layout.dialog_order_added, null);
        rootView.findViewById(R.id.btnYes).setOnClickListener(this);
        ((TextView)rootView.findViewById(R.id.orderNum)).setText("№" + order_num);

        if (message!=null) {
            ((TextView) rootView.findViewById(R.id.message)).setText(message);
        }
        return rootView;
    }

    public void onClick(View v) {
        listener.setTab(2);
        dismiss();
    }
}