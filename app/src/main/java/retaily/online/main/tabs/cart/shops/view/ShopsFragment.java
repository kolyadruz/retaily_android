package retaily.online.main.tabs.cart.shops.view;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import retaily.online.R;
import retaily.online.basicals.BaseFragment;
import retaily.online.main.tabs.cart.shops.adapter.ShopsAdapter;
import retaily.online.main.tabs.cart.shops.adapter.OnConfirmClickListener;
import retaily.online.main.adapter.OnFragmentsActionListener;
import retaily.online.main.tabs.cart.shops.adapter.OnShopItemClickListener;
import retaily.online.main.tabs.cart.shops.viewmodel.ShopsViewModel;
import retaily.online.models.local.Cart;
import retaily.online.models.request.CmdBody;
import retaily.online.models.request.ProductBody;
import retaily.online.models.response.getshops.Shop;
import retaily.online.service.EventObserver;
import retaily.online.utils.AppConstants;
import retaily.online.utils.PrefKeys;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ShopsFragment extends BaseFragment implements OnShopItemClickListener, OnConfirmClickListener {

    ShopsViewModel shopsViewModel;

    ShopsAdapter shopsAdapter;
    ArrayList<Shop> shops = new ArrayList<>();
    Shop selectedShop;

    RecyclerView shopsList;

    Context context;
    OnFragmentsActionListener listener;

    ArrayList<Cart> carts;

    String token;

    String supplierName = "";
    String supplierCode = "0";

    double summ = 0;

    Toolbar toolbar;

    public ShopsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            if (getArguments() != null) {

                ShopsFragmentArgs shopsFragmentArgs = ShopsFragmentArgs.fromBundle(getArguments());

                if (shopsFragmentArgs.getProducts() != null) {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<Cart>>() {
                    }.getType();
                    carts = gson.fromJson(shopsFragmentArgs.getProducts(), listType);
                }
                if (shopsFragmentArgs.getSupplierName() != null) {
                    supplierName = shopsFragmentArgs.getSupplierName();
                }
                if (shopsFragmentArgs.getSupplierCode() != null) {
                    supplierCode = shopsFragmentArgs.getSupplierCode();
                }

            }

            context = getActivity();
            listener = (OnFragmentsActionListener) getActivity();

            shopsViewModel = ViewModelProviders.of(getActivity()).get(ShopsViewModel.class);

            token = getActivity().getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE).getString(PrefKeys.TOKEN_KEY, "");

            CmdBody cmdBody = new CmdBody("getShops");
            cmdBody.setSupplier(supplierCode);
            shopsViewModel.getShops(token, cmdBody).observe(this, response -> {
                if (response.getError() == null) {
                    if (response.getResponse_code() == 200) {

                        shops.clear();
                        shops.addAll(response.getShops());

                        updateList();

                    } else {
                        handleError(context, response.getResponse_code(), response.getMessage(), true);
                    }
                } else {
                    Toast.makeText(context, ShopsFragment.class.getName() + ": " + response.getError(), Toast.LENGTH_SHORT).show();
                }
            });

            shopsViewModel.getSumInCart().observe(this, newSum -> {
                if (newSum != null) {
                    this.summ = newSum;
                }
            });
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_shops, container, false);

        shopsAdapter = new ShopsAdapter(shops,  carts != null ? this : null, context);
        shopsList = rootView.findViewById(R.id.shops_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        shopsList.setLayoutManager(linearLayoutManager);
        shopsList.setAdapter(shopsAdapter);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar = requireActivity().findViewById(R.id.toolbar_cart);
        toolbar.getMenu().clear();
    }

    private void updateList() {
        shopsAdapter.updateShops(shops);
    }

    @Override
    public void onClick(String shopCode) {
        for (Shop shop: shops) {
            if (shop.getCode().equals(shopCode)) {
                selectedShop = shop;
                break;
            }
        }
        listener.showConfirmDialog(this, selectedShop, summ, supplierCode);
    }

    @Override
    public void onConfirm(String supplierCode) {

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    String fireBaseToken = "";

                    if (task.isSuccessful() && task.getResult() != null) {
                        Log.d("Installations", "Installation fb token: " + task.getResult());
                        fireBaseToken = task.getResult();
                    } else {
                        //Log.e("Installations", "Unable to get Installation auth token");
                        if (getActivity() != null) {
                            Toast.makeText(getActivity().getApplicationContext(), "Не удалось получить токен FCM", Toast.LENGTH_LONG).show();
                        }
                    }

                    CmdBody cmdBody = new CmdBody("setOrders");
                    cmdBody.setFBtoken(fireBaseToken);
                    setOrder(supplierCode, cmdBody);

                });
    }

    private void setOrder(String supplierCode, CmdBody cmdBody) {

        ArrayList<ProductBody> productsBody = new ArrayList<>();

        for (Cart cart: carts) {
            productsBody.add(new ProductBody(cart.getProduct().getCode(), cart.getQuantity()));
        }

        String shop = selectedShop.getCode();

        cmdBody.setShop(shop);
        cmdBody.setPayment("1");
        cmdBody.setDelivery("1");
        cmdBody.setProducts(productsBody);
        cmdBody.setSupplier(supplierCode);

        shopsViewModel.setOrders(token, cmdBody).observe(getViewLifecycleOwner(), new EventObserver<>(response -> {
            if (response.getError() == null) {
                if (response.getResponse_code() == 200) {
                    shopsViewModel.deleteSupplierFromCart(supplierCode);
                    listener.showOrderAddedDialog(""+response.getOrder().getNum(), response.getOrder().getMessage());
                    Navigation.findNavController(requireActivity(), R.id.nav_host_cart).navigateUp();
                } else {
                    handleError(context, response.getResponse_code(), response.getMessage(), true);
                }
            } else {
                Toast.makeText(context, ShopsFragment.class.getName() + ": " + response.getError(), Toast.LENGTH_SHORT).show();
            }
        }));
    }
}