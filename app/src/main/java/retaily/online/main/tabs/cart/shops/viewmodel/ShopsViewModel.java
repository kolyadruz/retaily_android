package retaily.online.main.tabs.cart.shops.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import retaily.online.models.request.CmdBody;
import retaily.online.models.response.getshops.ShopResponse;
import retaily.online.models.response.setorders.SetOrdersResponse;
import retaily.online.repositories.MainRepository;
import retaily.online.service.Event;

public class ShopsViewModel extends AndroidViewModel {
    public ShopsViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<ShopResponse> getShops(String token, CmdBody cmdBody) {
        return MainRepository.getInstance().getShops(token, cmdBody);
    }

    public LiveData<Event<SetOrdersResponse>> setOrders(String token, CmdBody cmdBody) {
        return MainRepository.getInstance().setOrders(token, cmdBody);
    }

    public void deleteSupplierFromCart(String supplierName) {
        MainRepository.getInstance().deleteSupplierFromCart(supplierName);
    }

    public LiveData<Double> getSumInCart() {
        return MainRepository.getInstance().getAllProductsSumInCart();
    }
}
