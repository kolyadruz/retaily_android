package retaily.online.main.tabs.home.categories.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import retaily.online.R;
import retaily.online.models.response.getcategories.Category;
import retaily.online.utils.AppConstants;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    Context context;
    OnCategoryItemClickListener listener;
    ArrayList<Category> categories;

    public CategoriesAdapter(ArrayList<Category> categories, OnCategoryItemClickListener listener, Context context) {
        this.listener = listener;
        this.categories = categories;
        this.context = context;
    }

    @NonNull
    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category,parent,false);
        return new CategoriesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesAdapter.ViewHolder holder, int position) {

        Category category = categories.get(position);

        holder.categoryName.setText(category.getName());

        if (category.getImages().size() > 0) {
            Glide.with(context)
                    .load(AppConstants.SERVICE_URL + AppConstants.REPO + category.getImages().get(0) + "_webp")
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.categoryImage);
        } else {
            holder.categoryImage.setImageResource(R.mipmap.supplier_holder);
        }

        holder.setOnClickListener(categories.get(position).getCode());

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView categoryName;
        ImageView categoryImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryName = itemView.findViewById(R.id.category_name);
            categoryImage = itemView.findViewById(R.id.category_image);
        }

        private void setOnClickListener(String supplierCode) {
            itemView.setTag(supplierCode);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            listener.onCategoryClick((String) v.getTag());
        }
    }

    public void updateCategories(ArrayList<Category> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }
}