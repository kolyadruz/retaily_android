package retaily.online.main.tabs.home.categories.adapter;

public interface OnCategoryItemClickListener {

    void onCategoryClick(String categoryCode);

}
