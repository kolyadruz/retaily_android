package retaily.online.main.tabs.home.categories.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;
import com.google.gson.Gson;

import retaily.online.R;
import retaily.online.basicals.BaseFragment;
import retaily.online.main.tabs.home.categories.adapter.CategoriesAdapter;
import retaily.online.main.tabs.home.categories.adapter.OnCategoryItemClickListener;
import retaily.online.main.adapter.OnFragmentsActionListener;
import retaily.online.main.tabs.home.products.adapter.OnProductDetailClickListener;
import retaily.online.main.tabs.home.categories.viewmodel.CategoryViewModel;
import retaily.online.models.request.CmdBody;
import retaily.online.models.response.getcategories.Category;
import retaily.online.models.response.getstories.Story;
import retaily.online.models.response.getsuppliers.Supplier;
import retaily.online.service.EventObserver;
import retaily.online.utils.AppConstants;
import retaily.online.main.tabs.cart.cart.view.CartFragment;
import retaily.online.utils.PrefKeys;

import java.util.ArrayList;
import java.util.List;

public class CategoryFragment extends BaseFragment implements OnCategoryItemClickListener, OnProductDetailClickListener {

    Context context;
    OnFragmentsActionListener listener;
    CategoryViewModel categoryViewModel;

    String token;
    String supplierCode;
    String categoryCode;

    String supplierName;

    List<Story> stories = new ArrayList<>();

    int counter = 0;

    private ArrayList<Category> categories = new ArrayList<>();
    CategoriesAdapter categoriesAdapter;
    RecyclerView categoriesList;
    static  final int SPAN_COUNT = 2;

    ConstraintLayout rootLayout;
    View loading;
    boolean isLoading = false;

    Toolbar toolbar;
    ImageView logo;

    ImageView invisibleImageView;

    public CategoryFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (OnFragmentsActionListener)getActivity();
        this.context = context;
        categoryViewModel = ViewModelProviders.of(requireActivity()).get(CategoryViewModel.class);
        token = context.getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE).getString(PrefKeys.TOKEN_KEY, "");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            CategoryFragmentArgs categoryFragmentArgs = CategoryFragmentArgs.fromBundle(getArguments());
            supplierCode = categoryFragmentArgs.getSupplierCode();
            categoryCode = categoryFragmentArgs.getCategoryCode();
        }
        Supplier supplier = categoryViewModel.getSupplier(supplierCode);
        supplierName = supplier.getName();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_category, container, false);

        rootLayout = rootView.findViewById(R.id.root_layout);

        invisibleImageView = rootView.findViewById(R.id.invisible);

        loading = inflater.inflate(R.layout.loading, container, false);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        loading.setLayoutParams(layoutParams);

        categoriesList = rootView.findViewById(R.id.tagsTab);
        GridLayoutManager layoutManager2 = new GridLayoutManager(getActivity(), SPAN_COUNT);
        layoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
        categoriesList.setLayoutManager(layoutManager2);
        categoriesAdapter = new CategoriesAdapter(categories, this, getActivity());
        categoriesList.setAdapter(categoriesAdapter);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbar = requireActivity().findViewById(R.id.toolbar_suppliers);
        logo = requireActivity().findViewById(R.id.header_logo);
        logo.setVisibility(View.GONE);

        if (!isLoading) {
            rootLayout.addView(loading);
            isLoading = true;
        }

        CmdBody cmdBody = new CmdBody("getCategories", supplierCode);

        categoryViewModel.getCategories(token, cmdBody).observe(getViewLifecycleOwner(), new EventObserver<>(response -> {
            if (response.getError() == null) {
                if (response.getResponse_code() == 200) {
                    categories = response.getCategories();

                    categoryViewModel.addCategories(categories);

                    if (isLoading) {
                        rootLayout.removeView(loading);
                        isLoading = false;
                    }
                    updateList();
                } else {
                    handleError(context, response.getResponse_code(), response.getMessage(), true);
                }
            }
        }));
    }

    @Override
    public void onResume() {
        super.onResume();

        toolbar.setTitle(supplierName);
        if (!categoryCode.equals("") && !categoryCode.isEmpty()) {
            navigateToProducts(categoryCode);
            categoryCode = "";
        }

        CmdBody cmdBody = new CmdBody("getStories");
        cmdBody.setSupplier(supplierCode);
        categoryViewModel.getStories(token, cmdBody).observe(getViewLifecycleOwner(), storiesResponse -> {
            if (toolbar.getMenu() != null) {
                toolbar.getMenu().clear();
            }
            if (storiesResponse.getError() == null) {
                if (storiesResponse.getResponse_code() == 200) {
                    stories = storiesResponse.getData();
                    if (stories.size() > 0) {
                        for (Story story: stories) {
                            if (story.getImages() == null || story.getImages().size() == 0) {
                                stories.remove(story);
                            }
                        }
                        toolbar.inflateMenu(R.menu.category_menu);
                        toolbar.setOnMenuItemClickListener(item -> {
                            if (item.getItemId() == R.id.action_story) {
                                showStories();
                            }
                            return false;
                        });
                        counter = 0;
                        checkForNewStory();
                    }
                } else {
                    handleError(context, storiesResponse.getResponse_code(), storiesResponse.getMessage(), true);
                }
            }
        });
    }

    private void checkForNewStory() {
        List<String> images = stories.get(counter).getImages();
        if (images != null && images.size() > 0) {
            String imageurl = AppConstants.SERVICE_URL + AppConstants.REPO + images.get(0);
            Glide.with(requireActivity().getApplicationContext())
                    .load(imageurl)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .signature(new ObjectKey(imageurl))
                    //.skipMemoryCache(true)
                    .onlyRetrieveFromCache(true)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            boolean first = isFirstResource;
                            unviewedStoryFound();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            boolean first = isFirstResource;
                            if (stories.size() > 1 && counter < stories.size() - 1) {
                                counter++;
                                new Handler().post(() -> {
                                    checkForNewStory();
                                });
                            }
                            return false;
                        }
                    })
                    .into(invisibleImageView);
        }
    }

    private void unviewedStoryFound() {
        counter = 0;
        showStories();
    }

    private void showStories() {
        Gson gson = new Gson();
        Intent a = new Intent(context, StoriesActivity.class);
        a.putExtra("resources", gson.toJson(stories));
        startActivity(a);
    }

    private void updateList() {
        categoriesAdapter.updateCategories(categories);
    }

    @Override
    public void onCategoryClick(String categoryCode) {
        navigateToProducts(categoryCode);
    }

    private void navigateToProducts(String categoryCode) {
        NavDirections direction = CategoryFragmentDirections.actionNavCategoryToNavProducts(categoryCode, supplierCode, "");
        Navigation.findNavController(requireActivity(), R.id.nav_host_suppliers).navigate(direction);
    }

    @Override
    public void onProductDetailInCartClick() {
        Intent intent = new Intent(getActivity(), CartFragment.class);
        startActivity(intent);
    }
}