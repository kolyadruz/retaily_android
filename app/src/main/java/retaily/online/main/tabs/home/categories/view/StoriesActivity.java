package retaily.online.main.tabs.home.categories.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import jp.shts.android.storiesprogressview.StoriesProgressView;
import retaily.online.R;
import retaily.online.models.response.getstories.Story;
import retaily.online.utils.AppConstants;

public class StoriesActivity extends AppCompatActivity implements StoriesProgressView.StoriesListener {

    private StoriesProgressView storiesProgressView;
    private ImageView image;
    private ProgressBar spinner;

    private int counter = 0;

    private List<Story> stories = new ArrayList<>();

    long pressTime = 0L;
    long limit = 500L;

    private final View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();
                    storiesProgressView.pause();
                    return false;
                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();
                    storiesProgressView.resume();
                    return limit < now - pressTime;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_stories);

        if (getIntent() != null) {
            String resourcesStr = getIntent().getExtras().getString("resources");
            Gson gson = new Gson();
            stories = gson.fromJson(resourcesStr, new TypeToken<List<Story>>(){}.getType());
        }

        storiesProgressView = (StoriesProgressView) findViewById(R.id.stories);
        storiesProgressView.setStoriesCount(stories.size());
        storiesProgressView.setStoryDuration(5000L);
        storiesProgressView.setStoriesListener(this);

        spinner = findViewById(R.id.spinner);
        image = findViewById(R.id.image);

        ImageButton closeBtn = findViewById(R.id.close_btn);
        closeBtn.setOnClickListener(v -> finish());

        Button moreBtn = findViewById(R.id.moreBtn);
        moreBtn.setOnClickListener(view -> {
            String storyStr = new Gson().toJson(stories.get(counter));
            StoryDetailFragment storyDetailFragment = StoryDetailFragment.newInstance(storyStr);
            storyDetailFragment.show(getSupportFragmentManager(), storyDetailFragment.getTag());
            storiesProgressView.pause();
        });

        View reverse = findViewById(R.id.reverse);
        reverse.setOnClickListener(v -> storiesProgressView.reverse());
        reverse.setOnTouchListener(onTouchListener);

        View skip = findViewById(R.id.skip);
        skip.setOnClickListener(v -> storiesProgressView.skip());
        skip.setOnTouchListener(onTouchListener);

        checkInCache();
    }

    private void checkInCache() {
        spinner.setVisibility(View.INVISIBLE);

        String imageurl = AppConstants.SERVICE_URL + AppConstants.REPO + stories.get(counter).getImages().get(0);
        Glide.with(getApplicationContext())
                .load(imageurl)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .signature(new ObjectKey(imageurl))
                //.skipMemoryCache(true)
                .onlyRetrieveFromCache(true)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        new Handler().post(() -> updateImageView());
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        if (counter < stories.size()-1) {
                            counter++;
                            new Handler().post(() -> {
                                checkInCache();
                            });
                        } else {
                            counter = 0;
                            new Handler().post(() -> {
                                updateImageView();
                            });
                        }
                        return false;
                    }
                })
                .into(image);
    }

    private void updateImageView(){
        spinner.setVisibility(View.VISIBLE);
        storiesProgressView.pause();
        String imageurl = AppConstants.SERVICE_URL + AppConstants.REPO + stories.get(counter).getImages().get(0);
        Glide.with(getApplicationContext())
                .load(imageurl)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .signature(new ObjectKey(imageurl))
                //.skipMemoryCache(true)
                .fitCenter()
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        if (counter < stories.size()-1) {
                            counter++;
                            updateImageView();
                        } else {
                            finish();
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        startProgressView();
                        return false;
                    }
                })
                .into(image);
    }

    private void startProgressView(){
        spinner.setVisibility(View.GONE);
        storiesProgressView.destroy();
        storiesProgressView.startStories(counter);
    }

    @Override
    public void onNext() {
        //image.setImageResource(resources[++counter]);
        Log.d("STORY next", "" + counter);
        if (counter >= stories.size()-1) return;
        counter++;
        updateImageView();
    }

    @Override
    public void onPrev() {
        //if ((counter - 1) < 0) return;
        //image.setImageResource(resources[--counter]);
        Log.d("STORY prev", "" + counter);
        if ((counter - 1) < 0) return;
        counter--;
        updateImageView();
    }

    @Override
    public void onComplete() {
        finish();
    }

    @Override
    public void onResumeStory() {
        storiesProgressView.resume();
    }

    @Override
    protected void onDestroy() {
        // Very important !
        storiesProgressView.destroy();
        super.onDestroy();
    }
}