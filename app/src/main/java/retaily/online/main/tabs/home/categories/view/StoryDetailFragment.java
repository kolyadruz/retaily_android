package retaily.online.main.tabs.home.categories.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import jp.shts.android.storiesprogressview.StoriesProgressView;
import retaily.online.R;
import retaily.online.models.response.getstories.Story;

public class StoryDetailFragment extends BottomSheetDialogFragment {

    Story story;

    TextView storyMedia;
    Button openBtn;
    ImageButton closeBtn;

    StoriesProgressView.StoriesListener listener;

    private static final String KEY_STORY = "story_key";

    public StoryDetailFragment(){};

    public static StoryDetailFragment newInstance(String storyStr) {
        Bundle args = new Bundle();

        args.putString(KEY_STORY, storyStr);

        StoryDetailFragment fragment = new StoryDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (StoriesProgressView.StoriesListener)context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            story = new Gson().fromJson(getArguments().getString(KEY_STORY), Story.class);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bottom_sheet_story_detail, container, false);

        /**STORY_INFO**/
        storyMedia = rootView.findViewById(R.id.story_media);
        storyMedia.setText(story.getMedia());

        /**CLOSE_BTN**/
        closeBtn = rootView.findViewById(R.id.close_btn);
        closeBtn.setOnClickListener(view -> {
            dismissSelf();
            listener.onResumeStory();
        });

        /**OPEN_BTN**/
        openBtn = rootView.findViewById(R.id.open_btn);
        openBtn.setOnClickListener(view -> openProduct());

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCancel(@NonNull @NotNull DialogInterface dialog) {
        super.onCancel(dialog);
        listener.onResumeStory();
    }

    private void dismissSelf() {
        this.dismiss();
    }

    private void openProduct() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://retaily.online/lavka/"+story.getSupplier()+"/"+story.getCategory()+"/"+story.getProduct()));
        startActivity(browserIntent);
    }

}