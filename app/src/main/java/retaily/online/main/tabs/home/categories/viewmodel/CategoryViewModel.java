package retaily.online.main.tabs.home.categories.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

import retaily.online.models.request.CmdBody;
import retaily.online.models.response.getcategories.Category;
import retaily.online.models.response.getcategories.CategoryResponse;
import retaily.online.models.response.getstories.StoriesResponse;
import retaily.online.models.response.getsuppliers.Supplier;
import retaily.online.repositories.MainRepository;
import retaily.online.service.Event;

public class CategoryViewModel extends AndroidViewModel {

    public CategoryViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Event<CategoryResponse>> getCategories(String token, CmdBody cmdBody) {
        return MainRepository.getInstance().getCategories(token, cmdBody);
    }

    public Supplier getSupplier(String supplierCode) {
        return MainRepository.getInstance().getSupplier(supplierCode);
    }

    public LiveData<List<Long>> addCategories(ArrayList<Category> categories) {
        return MainRepository.getInstance().addCategories(categories);
    }

    public LiveData<StoriesResponse> getStories(String token, CmdBody cmdBody) {
        return MainRepository.getInstance().getStories(token, cmdBody);
    }
}
