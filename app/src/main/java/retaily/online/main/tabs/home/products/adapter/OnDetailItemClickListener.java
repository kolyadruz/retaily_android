package retaily.online.main.tabs.home.products.adapter;

public interface OnDetailItemClickListener {
    void onCartChangeBtnClicked(int position);
    void onInCartButtonClicked();

    void showCallForContractDialog();
}
