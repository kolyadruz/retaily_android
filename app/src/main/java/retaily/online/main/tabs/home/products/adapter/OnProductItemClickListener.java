package retaily.online.main.tabs.home.products.adapter;

import retaily.online.models.response.getproducts.Product;

public interface OnProductItemClickListener {

    void onProductClick(Product product, int itemPosition);

    long addToCart(Product product, long quantity);

    long checkAmountInCart(Product product);

    void deleteFromCart(Product product);

    void showCallToSupplierForContractDialog();
}
