package retaily.online.main.tabs.home.products.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import retaily.online.R;
import retaily.online.models.response.getproducts.Product;
import retaily.online.utils.AppConstants;

import java.util.ArrayList;

public class ProductsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    OnProductItemClickListener listener;
    ArrayList<Product> products;
    boolean hasContract;

    public ProductsAdapter(ArrayList<Product> products, OnProductItemClickListener listener, Context context, boolean hasContract) {
        this.listener = listener;
        this.context = context;
        this.products = products;
        this.hasContract = hasContract;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product,parent,false);
        return new ProductHolder(view2);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ProductHolder productHolder = (ProductHolder) holder;

        Product product = products.get(position);

        ArrayList<String> images = product.getImages();
        if (images.size() > 0) {
            Glide.with(context)
                    .load(AppConstants.SERVICE_URL + AppConstants.REPO + images.get(0) + "_webp")
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(productHolder.productImage);
        } else {
            productHolder.productImage.setImageResource(R.mipmap.supplier_holder);
        }

        productHolder.productName.setText(product.getName());
        productHolder.productDescShort.setText(product.getDescription_short());

        productHolder.quantity = productHolder.checkInCart(product);

        long quantum = product.getQuantum();
        long step = product.getStep();

        productHolder.productSummBtn.setVisibility(productHolder.quantity > 0 ? View.INVISIBLE : View.VISIBLE);
        productHolder.productPrice.setVisibility(productHolder.quantity > 0 ? View.VISIBLE : View.INVISIBLE);
        productHolder.plusMinusLayout.setVisibility(productHolder.quantity > 0 ? View.VISIBLE : View.INVISIBLE);

        productHolder.minusBtn.setOnClickListener(view -> {
            if (productHolder.quantity > quantum) {
                productHolder.addToCart(product, productHolder.quantity - step);
            } else {
                productHolder.deleteFromCart(product);
            }
            notifyItemChanged(position);
        });

        productHolder.plusBtn.setOnClickListener(view -> {
            if (productHolder.quantity < quantum) {
                productHolder.addToCart(product, productHolder.quantity + quantum);
            } else {
                if(product.getBalance() == 0) {
                    productHolder.addToCart(product, productHolder.quantity + step);
                } else if (productHolder.quantity < product.getBalance()) {
                    productHolder.addToCart(product, productHolder.quantity + step);
                }
            }
            notifyItemChanged(position);
        });

        productHolder.quantityTV.setText(""+productHolder.quantity);

        if (product.getBalance() != 0) {
            productHolder.productPrice.setText(product.getBalance() + "");
            productHolder.productSummBtn.setText(product.getBalance() + "");
        } else {
            productHolder.productPrice.setText(product.getPrice() + " ₽");
            productHolder.productSummBtn.setText(product.getPrice() + " ₽");
        }

        productHolder.productSummBtn.setOnClickListener(view -> {
            if (hasContract) {
                productHolder.addToCart(product, quantum);
                notifyItemChanged(position);
            } else {
                listener.showCallToSupplierForContractDialog();
            }
        });

        //productHolder.setOnClickListener(product.getCode());
        productHolder.productImage.setOnClickListener(view -> {
            productHolder.onProductClick(product, position);
        });

    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView productImage;
        TextView productName;
        TextView productDescShort;
        TextView productPrice;

        Button productSummBtn;

        ConstraintLayout plusMinusLayout;
        ImageButton minusBtn;
        ImageButton plusBtn;
        TextView quantityTV;

        public Long quantity;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.product_image);
            productName = itemView.findViewById(R.id.product_name);
            productDescShort = itemView.findViewById(R.id.product_desc_short);
            productPrice = itemView.findViewById(R.id.product_price);

            productSummBtn = itemView.findViewById(R.id.product_price_btn);

            plusMinusLayout = itemView.findViewById(R.id.plus_minus_layout);
            plusBtn = itemView.findViewById(R.id.plus_btn);
            minusBtn = itemView.findViewById(R.id.minus_btn);
            quantityTV = itemView.findViewById(R.id.quantity);

        }

        private void setOnClickListener(String productCode) {
            itemView.setTag(productCode);
            itemView.setOnClickListener(this);
        }

        private long checkInCart(Product product) {
            return listener.checkAmountInCart(product);
        }

        private long addToCart(Product product, long quantity) {
            return listener.addToCart(product, quantity);
        }

        private void deleteFromCart(Product product) {
            listener.deleteFromCart(product);
        }

        private void onProductClick(Product product, int position) {
            listener.onProductClick(product, position);
        }

        @Override
        public void onClick(View v) {
            //listener.onProductClick((String) v.getTag());
        }
    }

    public void updateProducts(ArrayList<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }
}