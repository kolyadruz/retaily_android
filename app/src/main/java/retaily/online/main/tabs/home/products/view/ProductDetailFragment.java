package retaily.online.main.tabs.home.products.view;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import retaily.online.R;
import retaily.online.main.tabs.home.products.adapter.OnDetailItemClickListener;
import retaily.online.main.tabs.home.products.viewmodel.ProductDetailViewModel;
import retaily.online.models.local.Cart;
import retaily.online.models.response.getproducts.Product;
import retaily.online.utils.AppConstants;

import java.util.ArrayList;

public class ProductDetailFragment extends BottomSheetDialogFragment {

    private OnDetailItemClickListener listener;
    private Context context;

    Product product;
    String supplierName;

    ImageView productImage;
    TextView productName;
    TextView productDescShort;
    TextView productDesc;

    ImageButton minusBtn;
    TextView quantityTV;
    ImageButton plusBtn;

    Button inCartBtn;
    Button productPriceBtn;

    ImageButton closeBtn;

    ProductDetailViewModel productDetailViewModel;

    LinearLayout plusMinusLayout;

    int itemPosition;
    boolean hasContract;

    private static final String KEY_PRODUCT = "product_key";
    private static final String KEY_SUPPLIER_NAME = "supplier_name_key";
    private static final String KEY_ITEM_POSITION = "item_position_key";
    private static final String KEY_HAS_CONTACT = "has_contact_key";

    public ProductDetailFragment(){}

    public static ProductDetailFragment newInstance(String product, String supplierName, int itemPosition, boolean hasContract) {
        Bundle args = new Bundle();

        args.putString(KEY_PRODUCT, product);
        args.putString(KEY_SUPPLIER_NAME, supplierName);
        args.putInt(KEY_ITEM_POSITION, itemPosition);
        args.putBoolean(KEY_HAS_CONTACT, hasContract);

        ProductDetailFragment fragment = new ProductDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        listener = (OnDetailItemClickListener)context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            product = new Gson().fromJson(getArguments().getString(KEY_PRODUCT), Product.class);
            supplierName = getArguments().getString(KEY_SUPPLIER_NAME);
            itemPosition = getArguments().getInt(KEY_ITEM_POSITION);
            hasContract = getArguments().getBoolean(KEY_HAS_CONTACT);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bottom_sheet_product_detail, container, false);

        /**IMAGE**/
        productImage = rootView.findViewById(R.id.product_image);
        ArrayList<String> images = product.getImages();
        if (images.size() > 0) {
            Glide.with(requireActivity())
                    .load(AppConstants.SERVICE_URL + AppConstants.REPO + images.get(0))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(productImage);
        } else {
            productImage.setImageResource(R.mipmap.supplier_holder);
        }

        /**PRODUCT_INFO**/
        productName = rootView.findViewById(R.id.product_name);
        productName.setText(product.getName());

        productDescShort = rootView.findViewById(R.id.product_desc_short);
        productDescShort.setText(product.getDescription_short());

        productDesc = rootView.findViewById(R.id.product_desc);
        productDesc.setText(product.getDescription());

        long quantum = product.getQuantum();

        /**MINUS_BTN**/
        minusBtn = rootView.findViewById(R.id.minus_button);
        minusBtn.setOnClickListener(view -> {
            long quantity = Long.parseLong(quantityTV.getText().toString());
            if (quantity > quantum) {
                addToCart(product, quantity - product.getStep());
            } else {
                deleteFromCart(product);
            }
            listener.onCartChangeBtnClicked(itemPosition);
        });

        /**AMOUNT**/
        quantityTV = rootView.findViewById(R.id.quantityTV);
        quantityTV.setOnClickListener(view -> showInputDialog(quantityTV.getText().toString()));

        /**PLUS_BTN**/
        plusBtn = rootView.findViewById(R.id.plus_button);
        plusBtn.setOnClickListener(view -> {
            long quantity = Long.parseLong(quantityTV.getText().toString());
            if (quantity < product.getQuantum()) {
                addToCart(product, quantity + quantum);
            } else {
                addToCart(product, quantity + product.getStep());
            }
            listener.onCartChangeBtnClicked(itemPosition);
        });

        /**CLOSE_BTN**/
        closeBtn = rootView.findViewById(R.id.close_button);
        closeBtn.setOnClickListener(view -> dismissSelf());

        /**IN_CART_TOTAL_SUMM_BTN**/
        inCartBtn = rootView.findViewById(R.id.in_cart_button);
        inCartBtn.setOnClickListener(view -> openCart());

        /**PRICE_BTN**/
        productPriceBtn = rootView.findViewById(R.id.product_price_btn);
        productPriceBtn.setText(product.getPrice() + " ₽");
        productPriceBtn.setOnClickListener(view -> {
            if (hasContract) {
                addToCart(product, (long) product.getQuantum());
                listener.onCartChangeBtnClicked(itemPosition);
            } else {
                listener.showCallForContractDialog();
                dismissSelf();
            }
        });

        plusMinusLayout = rootView.findViewById(R.id.plus_minus_layout);

        return rootView;
    }

    private void showInputDialog(String quantity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Введите желаемое количество");

        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.text_input_quantity, (ViewGroup) getView(), false);
        final EditText input = viewInflated.findViewById(R.id.input);
        input.setText(quantity);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            dialog.dismiss();
            roundValueAndSet(input.getText().toString());
        });
        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel());

        AlertDialog alertDialog = builder.create();

        alertDialog.setOnShowListener(dialogInterface -> {
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.btn_blue, null));
            alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.btn_blue, null));

        });

        alertDialog.show();
    }

    private void roundValueAndSet(String quantity) {
        if (quantity.length() > 0 && !quantity.equals("0")) {
            double q = Integer.parseInt(quantity);
            if (q > 0 && q > product.getQuantum()) {
                q = q - product.getQuantum();
                double divide = q / product.getStep();
                q = ((int)divide) * product.getStep();
                q = q + product.getQuantum();
            } else {
                q = product.getQuantum();
            }
            addToCart(product, (long)q);
            listener.onCartChangeBtnClicked(itemPosition);
            quantityTV.setText("" + (int)q);
        } else {
            deleteFromCart(product);
        }
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        productDetailViewModel = ViewModelProviders.of(requireActivity()).get(ProductDetailViewModel.class);
        productDetailViewModel.getAllProductsSumInCart().observe(this, sumInCart -> {
            if (sumInCart != null && sumInCart > 0) {
                inCartBtn.setText("В корзине " + String.format("%.2f", sumInCart) + " ₽");
                inCartBtn.setVisibility(View.VISIBLE);
            } else {
                inCartBtn.setVisibility(View.INVISIBLE);
            }
        });

        productDetailViewModel.checkDetailAmountInCart(product).observe(this, quantity -> {
            if (quantity != null) {
                quantityTV.setText(quantity + "");
                plusMinusLayout.setVisibility(quantity > 0 ? View.VISIBLE : View.GONE);
                productPriceBtn.setVisibility(quantity > 0 ? View.GONE : View.VISIBLE);
            } else {
                plusMinusLayout.setVisibility(View.GONE);
                productPriceBtn.setVisibility(View.VISIBLE);
            }
        });
    }

    private void dismissSelf() {
        this.dismiss();
    }

    private void openCart() {
        listener.onInCartButtonClicked();
        dismiss();
    }

    public long addToCart(Product product, Long quantity) {
        Cart cart  = productDetailViewModel.getCart(product);
        if (cart == null) cart = new Cart(product);
        cart.setQuantity(quantity);
        return productDetailViewModel.addToCart(cart);
    }

    public void deleteFromCart(Product product) {
        Cart cart = productDetailViewModel.getCart(product);
        productDetailViewModel.deleteFromCart(cart);
    }

}