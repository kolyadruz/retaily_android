package retaily.online.main.tabs.home.products.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retaily.online.R;
import retaily.online.basicals.BaseFragment;
import retaily.online.main.tabs.home.products.adapter.ProductsAdapter;
import retaily.online.main.tabs.home.products.adapter.OnDetailItemClickListener;
import retaily.online.main.adapter.OnFragmentsActionListener;
import retaily.online.main.tabs.home.products.adapter.OnProductDetailClickListener;
import retaily.online.main.tabs.home.products.adapter.OnProductItemClickListener;
import retaily.online.main.tabs.cart.cart.view.CartFragment;
import retaily.online.main.tabs.home.products.viewmodel.ProductViewModel;
import retaily.online.models.local.Cart;
import retaily.online.models.request.CmdBody;
import retaily.online.models.response.getcategories.Category;
import retaily.online.models.response.getproducts.Product;
import retaily.online.models.response.getsuppliers.Supplier;
import retaily.online.service.EventObserver;
import retaily.online.utils.AppConstants;

public class ProductFragment extends BaseFragment implements OnProductItemClickListener, OnProductDetailClickListener, OnDetailItemClickListener {

    Context context;
    OnFragmentsActionListener listener;
    ProductViewModel productViewModel;

    RecyclerView productsList;
    ProductsAdapter productsAdapter;
    GridLayoutManager gridLayoutManager;
    private final int SPAN_COUNT = 3;

    String token;
    String supplierCode;
    String categoryCode;
    String productCode;

    Supplier supplier;
    Category category;

    ConstraintLayout rootLayout;
    View loading;
    boolean isLoading = false;

    Toolbar toolbar;
    ImageView logo;

    public ProductFragment(){}

    @Override
    public void onAttach(@NonNull @NotNull Context context) {
        super.onAttach(context);
        listener = (OnFragmentsActionListener)getActivity();
        this.context = context;
        token = context.getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE).getString("token", "");

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity() != null) {

            if (getArguments() != null) {
                ProductFragmentArgs productFragmentArgs = ProductFragmentArgs.fromBundle(getArguments());

                categoryCode = productFragmentArgs.getCategoryCode();
                supplierCode = productFragmentArgs.getSupplierCode();
                productCode = productFragmentArgs.getProductCode();

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_product, container, false);

        rootLayout = rootView.findViewById(R.id.root_layout);

        loading = inflater.inflate(R.layout.loading, container, false);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        loading.setLayoutParams(layoutParams);

        productsList = rootView.findViewById(R.id.productsList);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        productViewModel = ViewModelProviders.of(requireActivity()).get(ProductViewModel.class);
        category = productViewModel.getCategory(categoryCode);
        supplier = productViewModel.getSupplier(supplierCode);

        toolbar = requireActivity().findViewById(R.id.toolbar_suppliers);
        logo = requireActivity().findViewById(R.id.header_logo);
        logo.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        toolbar.setTitle(category.getName());
        if (toolbar.getMenu() != null) {
            toolbar.getMenu().clear();
        }
        updateList();
    }

    private void updateList() {
        if (!isLoading) {
            rootLayout.addView(loading);
            isLoading = true;
        }

        String cmd = supplier.getType().equals("balance")? "getProductsBalance" : "getProducts";

        CmdBody cmdBody = new CmdBody(cmd, supplierCode, categoryCode);

        productViewModel.getProducts(token, cmdBody).observe(getViewLifecycleOwner(), new EventObserver<>(response -> {
            if (response.getError() == null) {
                if (response.getResponse_code() == 200) {
                    gridLayoutManager = new GridLayoutManager(context, SPAN_COUNT);
                    gridLayoutManager.setOrientation(RecyclerView.VERTICAL);

                    ArrayList<Product> products = response.getProducts();

                    if (!productCode.equals("")) {
                        for (Product product: products) {
                            if (product.getCode().equals(productCode)) {
                                productCode = "";
                                int itemPosition = products.indexOf(product);
                                gridLayoutManager.scrollToPositionWithOffset(itemPosition, 20);
                                listener.showProductDetail(this, product, supplier.getName(), itemPosition, supplier.isHasContract());
                                break;
                            }
                        }
                    }

                    productsList.setLayoutManager(gridLayoutManager);
                    productsAdapter = new ProductsAdapter(products,this, context, supplier.isHasContract());
                    productsList.setAdapter(productsAdapter);

                    if (isLoading) {
                        rootLayout.removeView(loading);
                        isLoading = false;
                    }

                } else {
                    handleError(context, response.getResponse_code(), response.getMessage(), true);
                }
            } else {
                Toast.makeText(context, ProductFragment.class.getSimpleName() + ": " + response.getError(), Toast.LENGTH_SHORT).show();
            }
        }));
    }

    @Override
    public void onProductClick(Product product, int itemPosition) {
        listener.showProductDetail(this, product, supplier.getName(), itemPosition, supplier.isHasContract());
    }

    @Override
    public long addToCart(Product product, long quantity) {
        if (supplier.isHasContract()) {
            Cart cart = productViewModel.getCart(product);
            if (cart == null) {
                cart = new Cart(product);
            }
            cart.setQuantity(quantity);
            return productViewModel.addToCart(cart);
        } else {
            listener.showCallSupplierForContractDialog(supplier.getPhone_support(), true);
        }
        return 0;
    }

    @Override
    public long checkAmountInCart(Product product) {
        return productViewModel.checkAmountInCart(product);
    }

    @Override
    public void deleteFromCart(Product product) {
        Cart cart = productViewModel.getCart(product);
        productViewModel.deleteFromCart(cart);
    }

    @Override
    public void showCallToSupplierForContractDialog() {
        listener.showCallSupplierForContractDialog(supplier.getPhone_support(), true);
    }

    @Override
    public void onProductDetailInCartClick() {
        Intent intent = new Intent(context, CartFragment.class);
        startActivity(intent);
    }

    @Override
    public void onCartChangeBtnClicked(int position) {
        productsAdapter.notifyItemChanged(position);
    }

    @Override
    public void onInCartButtonClicked() {
        listener.setTab(1);
    }

    @Override
    public void showCallForContractDialog() {
        listener.showCallSupplierForContractDialog(supplier.getPhone_support(), true);
    }
}