package retaily.online.main.tabs.home.products.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import retaily.online.models.local.Cart;
import retaily.online.models.response.getproducts.Product;
import retaily.online.repositories.MainRepository;

public class ProductDetailViewModel extends AndroidViewModel {
    public ProductDetailViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Double> getAllProductsSumInCart() {
        return MainRepository.getInstance().getAllProductsSumInCart();
    }

    public long addToCart(Cart cart) {
        return MainRepository.getInstance().addToCart(cart);
    }

    public Cart getCart(Product product) {
        return MainRepository.getInstance().getCart(product);
    }

    public void deleteFromCart(Cart cart) {
        MainRepository.getInstance().deleteFromCart(cart);
    }

    public LiveData<Long> checkDetailAmountInCart(Product product) {
        return MainRepository.getInstance().checkDetailInCart(product);
    }
}
