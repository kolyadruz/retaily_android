package retaily.online.main.tabs.home.products.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import retaily.online.models.local.Cart;
import retaily.online.models.request.CmdBody;
import retaily.online.models.response.getcategories.Category;
import retaily.online.models.response.getproducts.Product;
import retaily.online.models.response.getproducts.ProductResponse;
import retaily.online.models.response.getsuppliers.Supplier;
import retaily.online.repositories.MainRepository;
import retaily.online.service.Event;

public class ProductViewModel extends AndroidViewModel {

    public ProductViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Event<ProductResponse>> getProducts(String token, CmdBody cmdBody) {
        return MainRepository.getInstance().getProducts(token, cmdBody);
    }

    public long addToCart(Cart cart) {
        return MainRepository.getInstance().addToCart(cart);
    }

    public Cart getCart(Product product) {
        return MainRepository.getInstance().getCart(product);
    }

    public void deleteFromCart(Cart cart) {
        MainRepository.getInstance().deleteFromCart(cart);
    }

    public long checkAmountInCart(Product product) {
        return MainRepository.getInstance().checkInCart(product);
    }

    public Supplier getSupplier(String supplierCode) {
        return MainRepository.getInstance().getSupplier(supplierCode);
    }

    public Category getCategory(String categoryCode) {
        return MainRepository.getInstance().getCategory(categoryCode);
    }
}
