package retaily.online.main.tabs.home.suppliers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import retaily.online.R;
import retaily.online.models.response.getbanners.Banner;
import retaily.online.utils.AppConstants;

public class BannersAdapter extends RecyclerView.Adapter<BannersAdapter.ViewHolder> {

    Context context;
    OnBannerItemClickListener listener;
    ArrayList<Banner> banners;

    public BannersAdapter(ArrayList<Banner> banners, OnBannerItemClickListener listener, Context context) {
        this.listener = listener;
        this.banners = banners;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_banner,parent,false);
        GridLayoutManager.LayoutParams params =  (GridLayoutManager.LayoutParams) view.getLayoutParams();
        params.width = (parent.getMeasuredWidth() / 3) + (parent.getMeasuredWidth() / 12);
        view.setLayoutParams(params);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Banner banner = banners.get(position);

        ArrayList<String> images = banner.getImages();
        if (images.size() > 0) {
            Glide.with(context)
                    .load(AppConstants.SERVICE_URL + AppConstants.REPO + images.get(0))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.bannerImage);
        } else {
            holder.bannerImage.setImageResource(R.mipmap.supplier_holder);
        }

        holder.setOnClickListener(banner);

    }

    @Override
    public int getItemCount() {
        return banners.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView bannerImage;

        Banner banner;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            bannerImage =itemView.findViewById(R.id.banner_image);
        }

        private void setOnClickListener(Banner banner) {
            itemView.setTag(banner.getLink());
            this.banner = banner;
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            listener.onBannerClick(banner);
        }
    }

    public void updateArrays(ArrayList<Banner> banners) {
        this.banners = banners;
        notifyDataSetChanged();
    }
}