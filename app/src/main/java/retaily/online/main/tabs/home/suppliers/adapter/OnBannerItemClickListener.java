package retaily.online.main.tabs.home.suppliers.adapter;

import retaily.online.models.response.getbanners.Banner;

public interface OnBannerItemClickListener {

    void onBannerClick(Banner banner);

}
