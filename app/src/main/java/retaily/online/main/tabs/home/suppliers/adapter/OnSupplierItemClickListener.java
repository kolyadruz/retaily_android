package retaily.online.main.tabs.home.suppliers.adapter;

public interface OnSupplierItemClickListener {

    void onSupplierClick(String supplierCode);

}
