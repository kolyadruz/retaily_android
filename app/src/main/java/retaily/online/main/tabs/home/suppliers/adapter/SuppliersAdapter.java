package retaily.online.main.tabs.home.suppliers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.chip.ChipGroup;
import retaily.online.R;
import retaily.online.models.response.getsuppliers.Supplier;
import retaily.online.models.response.getsuppliers.Tag;
import retaily.online.utils.AppConstants;

import java.util.ArrayList;

public class SuppliersAdapter extends RecyclerView.Adapter<SuppliersAdapter.ViewHolder> {

    Context context;
    OnSupplierItemClickListener listener;
    ArrayList<Supplier> suppliers;
    ArrayList<Tag> tags;

    public SuppliersAdapter(ArrayList<Supplier> suppliers, ArrayList<Tag> tags, OnSupplierItemClickListener listener, Context context) {
        this.listener = listener;
        this.suppliers = suppliers;
        this.tags = tags;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_supplier,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Supplier supplier = suppliers.get(position);

        ArrayList<String> images = supplier.getImages();
        if (images.size() > 0) {
            Glide.with(context)
                    .load(AppConstants.SERVICE_URL + AppConstants.REPO + images.get(0))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.supplierImage);
        } else {
            holder.supplierImage.setImageResource(R.mipmap.supplier_holder);
        }

        holder.supplierNameTV.setText(supplier.getName());

        holder.supplierChips.removeAllViews();
        for (String supplierTag: supplier.getTags()) {
            for (Tag tag: tags) {
                if (supplierTag.equals(tag.getCode())) {
                    TextView chip = new TextView(context);
                    chip.setPadding(0,0,20,0);
                    //Chip chip = new Chip(context);
                    chip.setText(tag.getName());
                    holder.supplierChips.addView(chip);
                }
            }
        }

        holder.setOnClickListener(supplier.getCode(), supplier.getType(), supplier.getName(), supplier.isHasContract(), supplier.getPhone_support());

    }

    @Override
    public int getItemCount() {
        return suppliers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView supplierImage;
        String supplierCode;
        String supplierName;
        boolean hasContract;
        String support_phones;
        String supplierType;

        TextView supplierNameTV;
        ChipGroup supplierChips;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            supplierImage=itemView.findViewById(R.id.supplierImage);
            supplierNameTV=itemView.findViewById(R.id.supplier_name);
            supplierChips=itemView.findViewById(R.id.supplier_chips);
        }

        private void setOnClickListener(String supplierCode, String supplierType, String supplierName, boolean hasContract, String support_phones) {
            itemView.setTag(supplierCode);
            this.supplierCode = supplierCode;
            this.supplierName = supplierName;
            this.hasContract = hasContract;
            this.support_phones = support_phones;
            this.supplierType = supplierType;
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            listener.onSupplierClick(supplierCode);
        }
    }

    public void updateArrays(ArrayList<Supplier> suppliers, ArrayList<Tag> tags) {
        this.suppliers = suppliers;
        this.tags = tags;
        notifyDataSetChanged();
    }
}