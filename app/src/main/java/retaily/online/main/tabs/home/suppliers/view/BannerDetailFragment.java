package retaily.online.main.tabs.home.suppliers.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;

import java.util.ArrayList;

import retaily.online.R;
import retaily.online.models.response.getbanners.Banner;
import retaily.online.utils.AppConstants;

public class BannerDetailFragment extends BottomSheetDialogFragment {

    Banner banner;

    ImageView bannerImage;
    TextView bannerTitle;
    TextView bannerDesc;

    ImageButton closeBtn;

    Button moreBtn;

    private static final String KEY_BANNER = "banner_key";

    public BannerDetailFragment(){};

    public static BannerDetailFragment newInstance(String banner) {
        Bundle args = new Bundle();

        args.putString(KEY_BANNER, banner);

        BannerDetailFragment fragment = new BannerDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            banner = new Gson().fromJson(getArguments().getString(KEY_BANNER), Banner.class);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bottom_sheet_banner_detail, container, false);

        bannerImage = rootView.findViewById(R.id.banner_image);
        ArrayList<String> images = banner.getImages();

        if (images.size() > 0) {
            Glide.with(getActivity())
                    .load(AppConstants.SERVICE_URL + AppConstants.REPO + images.get(0))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(bannerImage);
        } else {
            bannerImage.setImageResource(R.mipmap.supplier_holder);
        }

        bannerTitle = rootView.findViewById(R.id.banner_title);
        bannerTitle.setText(banner.getTitle());

        bannerDesc = rootView.findViewById(R.id.banner_desc);
        bannerDesc.setText(banner.getContent());

        closeBtn = rootView.findViewById(R.id.banner_close_button);
        closeBtn.setOnClickListener(view -> {
            dismissSelf();
        });

        moreBtn = rootView.findViewById(R.id.more_button);
        if (banner.getLink() == null) {
            moreBtn.setVisibility(View.GONE);
        }
        moreBtn.setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(banner.getLink()));
            startActivity(browserIntent);
        });

        return rootView;
    }

    private void dismissSelf() {
        this.dismiss();
    }

}
