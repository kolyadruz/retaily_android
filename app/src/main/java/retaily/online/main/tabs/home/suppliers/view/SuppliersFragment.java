package retaily.online.main.tabs.home.suppliers.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import retaily.online.R;
import retaily.online.basicals.BaseFragment;
import retaily.online.main.tabs.home.suppliers.adapter.BannersAdapter;
import retaily.online.main.tabs.home.suppliers.adapter.OnBannerItemClickListener;
import retaily.online.main.tabs.home.suppliers.adapter.SuppliersAdapter;
import retaily.online.main.adapter.OnFragmentsActionListener;
import retaily.online.main.tabs.home.suppliers.adapter.OnSupplierItemClickListener;
import retaily.online.main.tabs.home.suppliers.viewmodel.SuppliersViewModel;
import retaily.online.models.request.CmdBody;
import retaily.online.models.response.getbanners.Banner;
import retaily.online.models.response.getsuppliers.Supplier;
import retaily.online.models.response.getsuppliers.Tag;
import retaily.online.service.EventObserver;
import retaily.online.utils.AppConstants;

import java.util.ArrayList;

public class SuppliersFragment extends BaseFragment implements OnSupplierItemClickListener, OnBannerItemClickListener {

    SuppliersAdapter suppliersAdapter;
    BannersAdapter bannersAdapter;

    private ArrayList<Tag> tags = new ArrayList<>();

    private ArrayList<Supplier> allSuppliers = new ArrayList<>();
    private final ArrayList<Supplier> suppliers = new ArrayList<>();

    SuppliersViewModel suppliersViewModel;

    Context context;
    OnFragmentsActionListener listener;
    String token;

    LinearLayoutManager layoutManager;

    TabLayout suppliersTagsTab;

    Toolbar toolbar;
    ImageView logo;

    ArrayList<Banner> banners = new ArrayList<>();

    public SuppliersFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        listener = (OnFragmentsActionListener) context;
        token = context.getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE).getString("token", "");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_suppliers, container, false);

        suppliersTagsTab = rootView.findViewById(R.id.tagsTab);
        suppliersTagsTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                suppliers.clear();
                if (position == 0) {
                    suppliers.addAll(allSuppliers);
                } else {
                    Tag tag = tags.get(position - 1);
                    for (Supplier supplier : allSuppliers) {
                        ArrayList<String> tags = supplier.getTags();
                        for (String supplierTag : tags) {
                            if (supplierTag.equals(tag.getCode())) {
                                suppliers.add(supplier);
                            }
                        }
                    }
                }
                updateSuppliersList();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        RecyclerView suppliersList = rootView.findViewById(R.id.suppliers_list);
        suppliersAdapter = new SuppliersAdapter(suppliers, tags,this, context);
        layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        suppliersList.setLayoutManager(layoutManager);
        suppliersList.setAdapter(suppliersAdapter);

        RecyclerView bannersList = rootView.findViewById(R.id.banners_list);
        bannersAdapter = new BannersAdapter(banners, this, context);
        GridLayoutManager mngr = new GridLayoutManager(context, 1);//LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
        mngr.setOrientation(RecyclerView.HORIZONTAL);
        bannersList.setLayoutManager(mngr);
        bannersList.setAdapter(bannersAdapter);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        suppliersViewModel = ViewModelProviders.of (requireActivity()).get(SuppliersViewModel.class);

        toolbar = requireActivity().findViewById(R.id.toolbar_suppliers);
        logo = requireActivity().findViewById(R.id.header_logo);

        logo.setVisibility(View.VISIBLE);
        toolbar.setTitle("");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (toolbar.getMenu() != null) {
            toolbar.getMenu().clear();
        }
        CmdBody cmdBody = new CmdBody("getSuppliers");
        suppliersViewModel.getSuppliersWithTags(token, cmdBody).observe(getViewLifecycleOwner(), new EventObserver<>(suppliersWithTags -> {
            if (suppliersWithTags == null) {
                return;
            }
            if (suppliersWithTags.getError() == null) {
                if  (suppliersWithTags.getResponse_code() == 200) {
                    tags = suppliersWithTags.getTags();

                    if (tags.size() > 0) {
                        suppliersTagsTab.removeAllTabs();
                        suppliersTagsTab.addTab(suppliersTagsTab.newTab().setText("Все"));
                        for (Tag tag : tags) {
                            suppliersTagsTab.addTab(suppliersTagsTab.newTab().setText(tag.getName()));
                        }
                    }

                    allSuppliers = suppliersWithTags.getSuppliers();

                    suppliers.clear();
                    suppliers.addAll(allSuppliers);

                    suppliersViewModel.addSuppliers(allSuppliers);

                    updateLists();

                } else {
                    //Toast.makeText(getActivity(), AppConstants.ON_TOKEN_EXPIRED, Toast.LENGTH_SHORT).show();
                    handleError(context, suppliersWithTags.getResponse_code(), suppliersWithTags.getMessage(), true);

                    listener.showLoginPage(true);
                }
            } else {
                Throwable e = suppliersWithTags.getError();
                Toast.makeText(getActivity(), "Error is " + e.getMessage(), Toast.LENGTH_SHORT).show();
                listener.showLoginPage(true);
            }
        }));

        cmdBody = new CmdBody("getBanners");
        suppliersViewModel.getBanners(token, cmdBody).observe(getViewLifecycleOwner(), bannersResponse -> {

            if (banners == null) {
                return;
            }

            if (bannersResponse.getError() == null) {
                if (bannersResponse.getResponse_code() == 200) {

                    banners = bannersResponse.getData();

                    updateBanners();

                } else {
                    handleError(context, bannersResponse.getResponse_code(), bannersResponse.getMessage(), true);
                }
            } else {
                Throwable e = bannersResponse.getError();
                Toast.makeText(getActivity(), "Error is " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });

    }

    private void updateLists() {
        updateSuppliersList();
    }

    private void updateBanners() {
        bannersAdapter.updateArrays(banners);
    }

    public void updateSuppliersList() {
        suppliersAdapter.updateArrays(suppliers, tags);
    }

    @Override
    public void onSupplierClick(String supplierCode) {
        NavDirections direction = SuppliersFragmentDirections.actionNavSuppliersToNavCategory(supplierCode, "");
        Navigation.findNavController(requireActivity(), R.id.nav_host_suppliers).navigate(direction);
    }

    @Override
    public void onBannerClick(Banner banner) {
        listener.showBannerDetail(banner);
    }
}