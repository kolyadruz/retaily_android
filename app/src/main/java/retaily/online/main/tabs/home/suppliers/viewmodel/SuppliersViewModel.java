package retaily.online.main.tabs.home.suppliers.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

import retaily.online.models.request.CmdBody;
import retaily.online.models.response.getbanners.BannerResponse;
import retaily.online.models.response.getsuppliers.Supplier;
import retaily.online.models.response.getsuppliers.SuppliersWithTags;
import retaily.online.repositories.MainRepository;
import retaily.online.service.Event;

public class SuppliersViewModel extends AndroidViewModel {

    public SuppliersViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Event<SuppliersWithTags>> getSuppliersWithTags(String token, CmdBody cmdBody) {
        return MainRepository.getInstance().getSuppliers(token, cmdBody);
    }

    public LiveData<BannerResponse> getBanners(String token, CmdBody cmdBody) {
        return MainRepository.getInstance().getBanners(token, cmdBody);
    }

    public LiveData<List<Long>> addSuppliers(ArrayList<Supplier> suppliers) {
        return MainRepository.getInstance().addSuppliers(suppliers);
    }

}
