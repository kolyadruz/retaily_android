package retaily.online.main.tabs.myorders.orderdetail.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.button.MaterialButton;

import retaily.online.R;
import retaily.online.models.local.CartHeader;
import retaily.online.models.response.TypedCartItem;
import retaily.online.models.response.getorders.OrderProduct;
import retaily.online.utils.AppConstants;

import java.util.ArrayList;

public class OrderDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<TypedCartItem> productsWithTitles;

    OrderDetailClickListener listener;

    public OrderDetailAdapter(ArrayList<TypedCartItem> productsWithTitles, Context context, OrderDetailClickListener listener) {
        this.context = context;
        this.productsWithTitles = productsWithTitles;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
            case 0:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_detail_title,parent,false);
                return new OrderDetailAdapter.ProductCategoryHolder(view);
            case 1:
                View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_detail_product,parent,false);
                return new OrderDetailAdapter.ProductHolder(view2);
            case 2:
                View view3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_detail_footer,parent,false);
                return new OrderDetailAdapter.ProductFooterHolder(view3);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case 0:
                OrderDetailAdapter.ProductCategoryHolder productCategoryHolder = (OrderDetailAdapter.ProductCategoryHolder) holder;
                CartHeader cartHeader = ((CartHeader) productsWithTitles.get(position));
                productCategoryHolder.supplierNameTV.setText(cartHeader.getProduct_supplier());

                String status = cartHeader.getStatus();
                productCategoryHolder.supplierCallLayout.setVisibility(status.equals("0") || status.equals("6") || status.equals("8") ? View.GONE : View.VISIBLE);
                String code = cartHeader.getProduct_supplier() != null ? cartHeader.getProduct_supplier() : "----";
                productCategoryHolder.codeSupplierTV.setText("Номер заказа: " + code);
                productCategoryHolder.callBtn.setOnClickListener(view -> listener.callSupplier(cartHeader.getPhone_support()));
                break;
            case 1:
                OrderDetailAdapter.ProductHolder productHolder = (OrderDetailAdapter.ProductHolder) holder;

                OrderProduct orderProduct = (OrderProduct)productsWithTitles.get(position);

                ArrayList<String> images = orderProduct.getImages();

                if (images.size() > 0) {
                    Glide.with(context)
                            .load(AppConstants.SERVICE_URL + AppConstants.REPO + images.get(0))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(productHolder.productImage);
                } else {
                    productHolder.productImage.setImageResource(R.mipmap.supplier_holder);
                }

                productHolder.productName.setText(orderProduct.getName());

                productHolder.quantity = orderProduct.getQuantity();

                productHolder.quantityTV.setText(orderProduct.getQuantity() + " шт");
                productHolder.productTotal.setVisibility(orderProduct.getTotal_cost() == 0 ? View.GONE : View.VISIBLE);
                productHolder.productTotal.setText(String.format("%.2f", orderProduct.getTotal_cost()) + " ₽");

                break;
            case 2:
                ProductFooterHolder productFooterHolder = (ProductFooterHolder) holder;
                //productCategoryHolder.totalTV.setText(String.format("%.2f", cartProductTitle.getTotal_cost()) + " ₽");
                productFooterHolder.totalTV.setText("ИТОГО: "+ String.format("%.2f", ((CartHeader)productsWithTitles.get(0)).getTotal_cost()) + " ₽");
                break;
            default:
                break;
        }

    }

    @Override
    public int getItemCount() {
        return productsWithTitles.size(); //+ 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < productsWithTitles.size()) {
            return productsWithTitles.get(position).getType();
        } return 2;
    }

    public class ProductCategoryHolder extends RecyclerView.ViewHolder {

        TextView supplierNameTV;
        TextView codeSupplierTV;
        MaterialButton callBtn;
        LinearLayout supplierCallLayout;

        public ProductCategoryHolder(@NonNull View itemView) {
            super(itemView);
            supplierNameTV = itemView.findViewById(R.id.supplierName);
            codeSupplierTV = itemView.findViewById(R.id.code_supplier);
            callBtn = itemView.findViewById(R.id.supplier_call);
            supplierCallLayout = itemView.findViewById(R.id.supplier_call_layout);
        }
    }

    public class ProductHolder extends RecyclerView.ViewHolder {

        ImageView productImage;
        TextView productName;
        TextView productTotal;
        TextView quantityTV;

        public long quantity;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.product_image);
            productName = itemView.findViewById(R.id.product_name);
            productTotal = itemView.findViewById(R.id.product_price);
            quantityTV = itemView.findViewById(R.id.quantityTV);
        }
    }

    public class ProductFooterHolder extends RecyclerView.ViewHolder {

        TextView totalTV;

        public ProductFooterHolder(@NonNull View itemView) {
            super(itemView);
            totalTV = itemView.findViewById(R.id.totalTV);
        }
    }

    public void updateProducts(ArrayList<TypedCartItem> productsWithTitles) {
        this.productsWithTitles = productsWithTitles;
        notifyDataSetChanged();
    }
}
