package retaily.online.main.tabs.myorders.orderdetail.view;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import retaily.online.R;
import retaily.online.basicals.BaseFragment;
import retaily.online.main.tabs.myorders.orderdetail.adapter.OrderDetailAdapter;
import retaily.online.main.adapter.OnFragmentsActionListener;
import retaily.online.main.tabs.myorders.orderdetail.adapter.OrderDetailClickListener;
import retaily.online.main.tabs.myorders.orderdetail.viewmodel.OrderDetailViewModel;
import retaily.online.main.tabs.myorders.orders.adapter.OnOrderItemClick;
import retaily.online.models.local.Cart;
import retaily.online.models.request.CmdBody;
import retaily.online.models.response.TypedCartItem;
import retaily.online.models.response.getorders.Order;
import retaily.online.models.response.getorders.OrderProduct;
import retaily.online.models.response.getproducts.Product;
import retaily.online.models.response.objects.status.Status;
import retaily.online.models.response.setorders.SetOrdersResponse;
import retaily.online.service.EventObserver;
import retaily.online.utils.AppConstants;
import retaily.online.utils.PrefKeys;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class OrderDetailFragment extends BaseFragment implements OrderDetailClickListener, OnOrderItemClick {

    Context context;
    OnFragmentsActionListener listener;
    OrderDetailViewModel orderDetailViewModel;

    String token;

    TextView supplierName;
    TextView dateTime;
    TextView supplierNum;
    TextView shopName;
    TextView statusTV;
    TextView quantity;
    Button call;
    Button cancel;

    RecyclerView ordersSsList;
    OrderDetailAdapter orderDetailAdapter;
    Button repeatBtn;

    ArrayList<TypedCartItem> typedProducts = new ArrayList<>();
    ArrayList<Status> statuses = new ArrayList<>();

    Order order;

    ConstraintLayout rootLayout;
    View loading;
    boolean isLoading = false;

    public OrderDetailFragment(){}

    @Override
    public void onAttach(@NonNull @NotNull Context context) {
        super.onAttach(context);
        this.context = context;
        listener = (OnFragmentsActionListener)context;
        token = context.getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE).getString(PrefKeys.TOKEN_KEY, "");

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            OrderDetailFragmentArgs orderDetailFragmentArgs = OrderDetailFragmentArgs.fromBundle(getArguments());

            Gson gson = new Gson();
            order = gson.fromJson(orderDetailFragmentArgs.getOrder(), Order.class);

            typedProducts.clear();
            typedProducts.addAll(order.getOrders_products());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_order_detail, container, false);

        rootLayout = rootView.findViewById(R.id.root_layout);
        loading = getLayoutInflater().inflate(R.layout.loading, rootLayout, false);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        loading.setLayoutParams(layoutParams);

        repeatBtn = rootView.findViewById(R.id.buttonRepeat);

        supplierName = rootView.findViewById(R.id.nameTV);
        supplierName.setText(order.getSupplier_name());

        String dateToShow = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
        try {
            Date formattedDate = sdf.parse(order.getDate_create());
            SimpleDateFormat formatToShow = new SimpleDateFormat("EE, dd MMM, HH:mm", Locale.getDefault());
            if (formattedDate != null) {
                dateToShow = formatToShow.format(formattedDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        dateTime = rootView.findViewById(R.id.dateTV);
        dateTime.setText(dateToShow);

        supplierNum = rootView.findViewById(R.id.supplierNumTV);
        String code = order.getCode_supplier() != null ? order.getCode_supplier() : "----";
        supplierNum.setText("Номер заказа: " + code);

        shopName = rootView.findViewById(R.id.shopNameTV);
        shopName.setText(order.getShop_name());

        quantity = rootView.findViewById(R.id.sumTV);
        quantity.setVisibility(order.getTotal_cost() == 0? View.GONE : View.VISIBLE);
        quantity.setText(String.format("%.2f", order.getTotal_cost()) + " ₽");

        call = rootView.findViewById(R.id.buttonCall);
        call.setVisibility(View.GONE);
        String status = order.getStatus();
        call.setVisibility(status.equals("0") || status.equals("6") || status.equals("8") ? View.GONE : View.VISIBLE);
        call.setOnClickListener(view -> callSupplier(order.getSupplier_phone_support()));

        cancel = rootView.findViewById(R.id.buttonCancel);
        cancel.setVisibility(View.GONE);

        statusTV = rootView.findViewById(R.id.statusTV);

        orderDetailAdapter = new OrderDetailAdapter(typedProducts, context, this);
        ordersSsList = rootView.findViewById(R.id.order_suppliers_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        ordersSsList.setLayoutManager(linearLayoutManager);
        ordersSsList.setAdapter(orderDetailAdapter);

        return rootView;
    }

    private void setStatus(String status) {
        for (Status statusObj: statuses) {
            if (statusObj.getCode().equals(status)) {
                statusTV.setText(statusObj.getName());
                statusTV.setTextColor(Color.parseColor(statusObj.getColor()));
                cancel.setVisibility(statusObj.isIs_cancelable() ? View.VISIBLE : View.GONE);
                break;
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        orderDetailViewModel = ViewModelProviders.of(requireActivity()).get(OrderDetailViewModel.class);

        orderDetailViewModel.getStatuses().observe(getViewLifecycleOwner(), newStatuses -> {
            statuses.clear();
            statuses.addAll(newStatuses);

            if (cancel != null) {
                setStatus(order.getStatus());
            }

        });


        repeatBtn.setOnClickListener(view -> {
            if (getActivity() != null) {
                CmdBody cmdBody = new CmdBody("repeatOrder");
                cmdBody.setOrder(order.getCode());
                if (!isLoading) {
                    isLoading = true;
                    rootLayout.addView(loading);
                }

               orderDetailViewModel.repeatOrder(token, cmdBody, order.getOrders_products().get(0).getBalance() > 0).observe(getViewLifecycleOwner(), new EventObserver<>(this::updateCart));
            }
        });

        cancel.setOnClickListener(view -> {
            if (!isLoading) {
                isLoading = true;
                rootLayout.addView(loading);
            }
            cancelOrder(order.getNum(), order.getCode(), order.getOrders_products().get(0).getBalance());
        });
    }

    private void updateCart(SetOrdersResponse response) {
        if (isLoading) {
            isLoading = false;
            rootLayout.removeView(loading);
        }

        if (response.getError() == null) {
            if(response.getResponse_code() == 200) {

                orderDetailViewModel.clearAllFromCart();

                ArrayList<Cart> carts = new ArrayList<>();

                Order actualOrder = response.getOrder();

                for (OrderProduct orderProduct : actualOrder.getOrders_products()) {
                    Product product = new Product();
                    product.setCode(orderProduct.getProduct());
                    product.setName(orderProduct.getName());
                    product.setQuantum(orderProduct.getQuantum());
                    product.setSupplier(actualOrder.getSupplier());
                    product.setDescription_short(orderProduct.getDescription_short());
                    product.setPrice(orderProduct.getPrice());
                    product.setImages(orderProduct.getImages());
                    product.setBalance(orderProduct.getBalance());
                    product.setStep(orderProduct.getStep());

                    carts.add(new Cart(product, orderProduct.getQuantity()));
                }

                orderDetailViewModel.addToCart(carts).observe(getViewLifecycleOwner(), cartIds -> {
                    listener.setTab(1);
                    Navigation.findNavController(requireActivity(), R.id.nav_host_orders).navigateUp();
                });
            } else {
                handleError(context, response.getResponse_code(), response.getMessage(), true);
            }
        } else {
            Toast.makeText(getActivity(), OrderDetailFragment.class.getName() + ": " + response.getError(), Toast.LENGTH_SHORT).show();
        }
    }

    private void updateList() {
        orderDetailAdapter.updateProducts(typedProducts);
    }

    @Override
    public void callSupplier(String phones) {
        listener.showCallSupplierForContractDialog(phones, false);
    }

    @Override
    public void onClick(Order order) {

    }

    @Override
    public void cancelOrder(int orderNum, String orderCode, long balance) {
        listener.showDeleteDialog(orderNum, orderCode, balance, this);
    }

    @Override
    public void onConfirmCancel(String orderCode, long balance) {
        if (!isLoading) {
            rootLayout.addView(loading);
            isLoading = true;
        }
        if (getActivity() != null) {
            String token = getActivity().getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE).getString(PrefKeys.TOKEN_KEY, "");
            CmdBody cmdBody = new CmdBody("cancelorder");
            cmdBody.setOrder(order.getCode());

            orderDetailViewModel.cancelOrder(token, cmdBody, balance > 0).observe(getViewLifecycleOwner(), s -> {
                Navigation.findNavController(requireActivity(), R.id.nav_host_orders).navigateUp();
            });
        }
    }
}
