package retaily.online.main.tabs.myorders.orderdetail.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import retaily.online.models.local.Cart;
import retaily.online.models.request.CmdBody;
import retaily.online.models.response.objects.status.Status;
import retaily.online.models.response.setorders.SetOrdersResponse;
import retaily.online.repositories.MainRepository;
import retaily.online.service.Event;

import java.util.List;

public class OrderDetailViewModel extends AndroidViewModel {
    public OrderDetailViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<Status>> getStatuses() {
        return MainRepository.getInstance().getStatuses();
    }

    public void clearAllFromCart() {
        MainRepository.getInstance().clearAllFromCart();
    }

    public LiveData<List<Long>> addToCart(List<Cart> carts) {
        return MainRepository.getInstance().addToCart(carts);
    }

    public LiveData<Event<SetOrdersResponse>> repeatOrder(String bearerToken, CmdBody cmdBody, boolean isBalance) {
        if (isBalance) {
            return MainRepository.getInstance().repeatOrderBalance(bearerToken, cmdBody);
        } else {
            return MainRepository.getInstance().repeatOrder(bearerToken, cmdBody);
        }
    }

    public LiveData<Event<String>> cancelOrder(String token, CmdBody cmdBody, boolean isBalance) {
        if (isBalance) {
            return MainRepository.getInstance().cancelOrderBalance(token, cmdBody);
        } else {
            return MainRepository.getInstance().cancelOrder(token, cmdBody);
        }
    }
}
