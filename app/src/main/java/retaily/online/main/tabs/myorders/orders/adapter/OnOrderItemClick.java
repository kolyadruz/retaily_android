package retaily.online.main.tabs.myorders.orders.adapter;

import retaily.online.models.response.getorders.Order;

public interface OnOrderItemClick {
    void onClick(Order order);

    void cancelOrder(int orderNum, String orderCode, long balance);
    void onConfirmCancel(String orderCode, long balance);
}
