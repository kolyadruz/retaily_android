package retaily.online.main.tabs.myorders.orders.adapter;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retaily.online.models.request.CmdBody;
import retaily.online.models.response.getorders.Order;
import retaily.online.repositories.MainRepository;
import retaily.online.service.IYakitProService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDataSource extends PageKeyedDataSource<Long, Order> {

    static final int ROW_COUNT = 10;

    String token;

    IYakitProService iYakitProService;

    OrderDataSource(String token) {
        this.token = token;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull LoadInitialCallback<Long, Order> callback) {
        iYakitProService = MainRepository.getService();
        if (iYakitProService != null) {

            CmdBody cmdBody = new CmdBody("getOrders");
            cmdBody.setRowCount(ROW_COUNT);
            cmdBody.setRowSkip(0);

            iYakitProService.getOrders("Bearer " + token, cmdBody)
                    .enqueue(new Callback<List<Order>>() {
                        @Override
                        public void onResponse(@NotNull Call<List<Order>> call, @NotNull Response<List<Order>> response) {
                            if (response.body() != null)
                                callback.onResult(response.body(), null, (long) 2);

                        }

                        @Override
                        public void onFailure(@NotNull Call<List<Order>> call, @NotNull Throwable t) {

                        }
                    });
        }


    }

    @Override
    public void loadBefore(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, Order> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, Order> callback) {

        iYakitProService = MainRepository.getService();
        if (iYakitProService != null) {

            CmdBody cmdBody = new CmdBody("getOrders");
            cmdBody.setRowCount(ROW_COUNT);
            cmdBody.setRowSkip((int) ((params.key - 1) * ROW_COUNT));

            iYakitProService.getOrders("Bearer " + token, cmdBody)
                    .enqueue(new Callback<List<Order>>() {
                        @Override
                        public void onResponse(@NotNull Call<List<Order>> call, @NotNull Response<List<Order>> response) {
                            if (response.body() != null)
                                callback.onResult(response.body(), params.key + 1);
                        }

                        @Override
                        public void onFailure(@NotNull Call<List<Order>> call, @NotNull Throwable t) {

                        }
                    });
        }

    }
}
