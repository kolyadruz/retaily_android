package retaily.online.main.tabs.myorders.orders.adapter;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

public class OrderDataSourceFactory  extends DataSource.Factory {

    String token;

    OrderDataSource orderDataSource;
    MutableLiveData<OrderDataSource> mutableLiveData;

    public OrderDataSourceFactory(String token) {
        this.token = token;
        mutableLiveData = new MutableLiveData<>();
    }

    @Override
    public DataSource create() {
        orderDataSource = new OrderDataSource(token);
        mutableLiveData.postValue(orderDataSource);
        return orderDataSource;
    }

    public MutableLiveData<OrderDataSource> getMutableLiveData() {
        return mutableLiveData;
    }

    public void refresh() {
        mutableLiveData.getValue().invalidate();
    }
}