package retaily.online.main.tabs.myorders.orders.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import retaily.online.R;
import retaily.online.models.response.getorders.Order;
import retaily.online.models.response.objects.status.Status;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class OrdersAdapter extends PagedListAdapter<Order, OrdersAdapter.OrderViewHolder> {

    OnOrderItemClick listener;
    ArrayList<Status> statuses = new ArrayList<>();

    public OrdersAdapter(OnOrderItemClick listener, ArrayList<Status> statuses) {
        super(Order.CALLBACK);
        this.listener = listener;
        this.statuses = statuses;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent, false);
        return new OrderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {

        //Order order = orders.get(position);

        Order order = getItem(position);
        holder.order = order;

        holder.numTV.setText("Заявка №" + order.getNum());

        String date = order.getDate_create();
        String dateToShow = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
        try {
            Date formattedDate = sdf.parse(date);
            if (formattedDate != null) {
                SimpleDateFormat formatToShow = new SimpleDateFormat("EE, dd MMM, HH:mm", Locale.getDefault());
                dateToShow = formatToShow.format(formattedDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.dateTV.setText(dateToShow);

        holder.shopNameTV.setText(order.getShop_name());

        for (Status status: statuses) {
            if (status.getCode().equals(order.getStatus())) {
                holder.statusTV.setText(status.getName());

                holder.statusTV.setTextColor(Color.parseColor(status.getColor()));
                holder.cancelBtn.setVisibility(status.isIs_cancelable() ? View.VISIBLE : View.GONE);

                break;
            }
        }

        holder.supplierName.setText(order.getSupplier_name());

        holder.sumTV.setVisibility(order.getTotal_cost() == 0 ? View.GONE : View.VISIBLE);
        holder.sumTV.setText(String.format("%.2f", order.getTotal_cost()) + " ₽");

        holder.setOnClickListener(order.getCode());

        holder.cancelBtn.setOnClickListener(view -> {
            listener.cancelOrder(order.getNum(), order.getCode(), order.getOrders_products().get(0).getBalance());
        });
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Order order;

        TextView numTV;
        TextView dateTV;
        TextView statusTV;
        TextView supplierName;
        TextView shopNameTV;

        TextView sumTV;

        ImageButton cancelBtn;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);
            numTV = itemView.findViewById(R.id.numTV);
            dateTV = itemView.findViewById(R.id.dateTV);
            statusTV = itemView.findViewById(R.id.statusTV);
            supplierName = itemView.findViewById(R.id.supplierNameTV);
            shopNameTV = itemView.findViewById(R.id.shopNameTV);
            sumTV = itemView.findViewById(R.id.sumTV);

            cancelBtn = itemView.findViewById(R.id.cancelBtn);
        }

        private void setOnClickListener(String code) {
            itemView.setTag(code);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(order);
        }

    }
}