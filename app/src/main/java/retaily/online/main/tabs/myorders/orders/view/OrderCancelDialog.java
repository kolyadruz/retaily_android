package retaily.online.main.tabs.myorders.orders.view;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import retaily.online.R;

import retaily.online.main.tabs.myorders.orders.adapter.OnOrderItemClick;

public class OrderCancelDialog extends DialogFragment implements View.OnClickListener {

    final String LOG_TAG = "myLogs";

    int order_num;
    String order_code;
    long balance;

    OnOrderItemClick listener;

    private static final String KEY_ORDER_NUM = "order_num_key";
    private static final String KEY_ORDER_CODE = "order_code_key";
    private static final String KEY_ORDER_BALANCE = "order_balance_key";

    public OrderCancelDialog(){}

    public static OrderCancelDialog newInstance(int order_num, String order_code, long balance) {
        Bundle args = new Bundle();

        args.putInt(KEY_ORDER_NUM, order_num);
        args.putString(KEY_ORDER_CODE, order_code);
        args.putLong(KEY_ORDER_BALANCE, balance);

        OrderCancelDialog fragment = new OrderCancelDialog();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (OnOrderItemClick) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            order_num = getArguments().getInt(KEY_ORDER_NUM);
            order_code = getArguments().getString(KEY_ORDER_CODE);
            balance = getArguments().getLong(KEY_ORDER_BALANCE);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Успешно!");
        View rootView = inflater.inflate(R.layout.dialog_order_cancel, null);
        rootView.findViewById(R.id.btnYes).setOnClickListener(this);
        rootView.findViewById(R.id.btnNo).setOnClickListener(view -> dismiss());
        ((TextView)rootView.findViewById(R.id.orderNum)).setText("№" + order_num + "?");
        return rootView;
    }

    public void onClick(View v) {
        listener.onConfirmCancel(order_code, balance);
        dismiss();
    }
}