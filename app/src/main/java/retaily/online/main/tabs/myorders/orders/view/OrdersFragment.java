package retaily.online.main.tabs.myorders.orders.view;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import retaily.online.R;
import retaily.online.basicals.BaseFragment;
import retaily.online.main.tabs.myorders.orders.adapter.OrdersAdapter;
import retaily.online.main.adapter.OnFragmentsActionListener;
import retaily.online.main.tabs.myorders.orders.adapter.OnOrderItemClick;
import retaily.online.main.tabs.myorders.orders.viewmodel.OrdersViewModelFactory;
import retaily.online.main.tabs.myorders.orders.viewmodel.OrdersViewModel;
import retaily.online.models.request.CmdBody;
import retaily.online.models.response.getorders.Order;
import retaily.online.models.response.objects.status.Status;
import retaily.online.utils.AppConstants;
import retaily.online.utils.PrefKeys;

import java.util.ArrayList;

public class OrdersFragment extends BaseFragment implements OnOrderItemClick {

    Context context;
    OnFragmentsActionListener listener;
    private OrdersViewModel ordersViewModel;

    RecyclerView ordersList;
    OrdersAdapter ordersAdapter;

    SwipeRefreshLayout rootLayout;
    View loading;
    boolean isLoading = false;

    ArrayList<Status> statuses = new ArrayList<>();

    public OrdersFragment(){}

    String token;
    Application application;

    @Override
    public void onAttach(@NonNull @NotNull Context context) {
        super.onAttach(context);
        token = context.getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE).getString(PrefKeys.TOKEN_KEY, "");
        application = ((Activity)context).getApplication();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            context = getActivity();
            listener = (OnFragmentsActionListener)getActivity();
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_orders, container, false);

        rootLayout = rootView.findViewById(R.id.root_layout);
        rootLayout.setOnRefreshListener(this::refreshList);

        loading = inflater.inflate(R.layout.loading, container, false);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        loading.setLayoutParams(layoutParams);

        ordersList = rootView.findViewById(R.id.orders_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        ordersList.setLayoutManager(linearLayoutManager);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ordersViewModel = new ViewModelProvider(this, new OrdersViewModelFactory(application, token)).get(OrdersViewModel.class);
        ordersViewModel.getStatuses().observe(getViewLifecycleOwner(), newStatuses -> {
            statuses.clear();
            statuses.addAll(newStatuses);
        });
        ordersViewModel.getOrdersLiveData().observe(getViewLifecycleOwner(), orders -> {
            rootLayout.setRefreshing(false);
            // Adapter
            ordersAdapter = new OrdersAdapter(this, statuses);
            ordersAdapter.submitList(orders);

            // RecyclerView
            ordersList.setAdapter(ordersAdapter);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onClick(Order order) {
        Gson gson = new Gson();
        String jsonOrder = gson.toJson(order);

        NavDirections direction = OrdersFragmentDirections.actionNavOrdersToNavOrderSuppliers(jsonOrder, ""+order.getNum());
        if (getView() != null) Navigation.findNavController(getView()).navigate(direction);
    }

    @Override
    public void cancelOrder(int orderNum, String orderCode, long balance) {
        listener.showDeleteDialog(orderNum, orderCode, balance, this);
    }

    @Override
    public void onConfirmCancel(String orderCode, long balance) {
        if (!isLoading) {
            rootLayout.addView(loading);
            isLoading = true;
        }
        if (getActivity() != null) {
            String token = getActivity().getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE).getString(PrefKeys.TOKEN_KEY, "");
            CmdBody cmdBody = new CmdBody("cancelorder");
            cmdBody.setOrder(orderCode);

            ordersViewModel.cancelOrder(token, cmdBody, balance > 0).observe(getViewLifecycleOwner(), s -> {
                refreshList();
            });
        }
    }

    private void refreshList() {
        rootLayout.setRefreshing(true);
        ordersViewModel.orderDataSourceFactory.refresh();
    }
}