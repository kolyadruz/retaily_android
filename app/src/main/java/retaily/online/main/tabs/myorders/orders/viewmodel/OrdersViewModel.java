package retaily.online.main.tabs.myorders.orders.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import retaily.online.main.tabs.myorders.orders.adapter.OrderDataSource;
import retaily.online.main.tabs.myorders.orders.adapter.OrderDataSourceFactory;
import retaily.online.models.request.CmdBody;
import retaily.online.models.response.getorders.Order;
import retaily.online.models.response.objects.status.Status;
import retaily.online.repositories.MainRepository;
import retaily.online.service.Event;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class OrdersViewModel extends AndroidViewModel {

    public OrderDataSourceFactory orderDataSourceFactory;
    MutableLiveData<OrderDataSource> orderDataSourceMutableLiveData;
    Executor executor;
    LiveData<PagedList<Order>> pagedListLiveData;

    public OrdersViewModel(@NonNull Application application, String token) {
        super(application);
        orderDataSourceFactory = new OrderDataSourceFactory(token);
        orderDataSourceMutableLiveData = orderDataSourceFactory.getMutableLiveData();

        PagedList.Config config = (new PagedList.Config.Builder())
                .setEnablePlaceholders(true)
                .setInitialLoadSizeHint(10)
                .setPageSize(20)
                .setPrefetchDistance(4)
                .build();
        executor = Executors.newFixedThreadPool(5);
        pagedListLiveData = (new LivePagedListBuilder<Long, Order>(orderDataSourceFactory, config))
                .setFetchExecutor(executor)
                .build();
    }

    /*public LiveData<Event<OrdersResponse>> getOrders(Long position) {
        CmdBody cmdBody = new CmdBody("getOrders");
        return MainRepository.getInstance().getOrders(token, cmdBody);
    }*/

    public LiveData<PagedList<Order>> getOrdersLiveData() {
        return pagedListLiveData;
    }

    public LiveData<List<Status>> getStatuses() {
        return MainRepository.getInstance().getStatuses();
    }

    public LiveData<Event<String>> cancelOrder(String token, CmdBody cmdBody, boolean isBalance) {
        if (isBalance) {
            return MainRepository.getInstance().cancelOrderBalance(token, cmdBody);
        } else {
            return MainRepository.getInstance().cancelOrder(token, cmdBody);
        }
    }
}