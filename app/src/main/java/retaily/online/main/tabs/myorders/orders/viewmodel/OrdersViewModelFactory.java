package retaily.online.main.tabs.myorders.orders.viewmodel;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class OrdersViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private String token;


    public OrdersViewModelFactory(Application application, String token) {
        mApplication = application;
        this.token = token;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new OrdersViewModel(mApplication, token);
    }
}
