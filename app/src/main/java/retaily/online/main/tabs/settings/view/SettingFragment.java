package retaily.online.main.tabs.settings.view;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import org.jetbrains.annotations.NotNull;

import retaily.online.BuildConfig;
import retaily.online.R;
import retaily.online.main.adapter.OnFragmentsActionListener;
import retaily.online.utils.AppConstants;
import retaily.online.main.tabs.settings.viewmodel.SettingsViewModel;
import retaily.online.utils.PrefKeys;

public class SettingFragment extends Fragment {

    OnFragmentsActionListener listener;
    private SettingsViewModel settingsViewModel;

    String user_id;

    TextView userName;
    TextView shopsTV;
    TextView exitTV;
    TextView supportTV;
    TextView versionName;

    @Override
    public void onAttach(@NonNull @NotNull Context context) {
        super.onAttach(context);
        listener = (OnFragmentsActionListener)context;
        user_id = context.getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE).getString(PrefKeys.CURRENT_USER_ID_KEY, "");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        userName = rootView.findViewById(R.id.user_name);

        versionName = rootView.findViewById(R.id.versionName);
        versionName.setText("ver " + BuildConfig.VERSION_NAME);

        shopsTV = rootView.findViewById(R.id.shopsTV);
        shopsTV.setOnClickListener(view -> {
            NavDirections direction =  SettingFragmentDirections.actionSettingsDestToShopsFragment(null, null, null);
            Navigation.findNavController(requireActivity(), R.id.nav_host_settings).navigate(direction);
        });

        supportTV = rootView.findViewById(R.id.supportTV);
        supportTV.setOnClickListener(view -> {
            showCallDialog();
        });

        exitTV = rootView.findViewById(R.id.exitTV);
        exitTV.setOnClickListener(view -> {
            showConfirmExitDialog();
        });

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        settingsViewModel = ViewModelProviders.of(requireActivity()).get(SettingsViewModel.class);
        settingsViewModel.getUserInfo(user_id).observe(getViewLifecycleOwner(), userInfo -> userName.setText(userInfo.name));
    }

    private void showConfirmExitDialog() {

        AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
        adb.setTitle("Вы уверены что хотите выйти из аккаунта?");
        adb.setPositiveButton("Да", (dialog, which) -> listener.showLoginPage(false));
        adb.setNegativeButton("Нет", (dialog, which) -> {
        });

        AlertDialog dialog = adb.create();

        dialog.show();

        Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(getResources().getColor(R.color.btn_blue));
        Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(getResources().getColor(R.color.btn_blue));

    }

    private void showCallDialog() {
        String phones = AppConstants.SUPPORT_PHONE;
        String[] separated = phones.split(",");

        AlertDialog.Builder phoneNumbersAlert = new AlertDialog.Builder(getContext());
        phoneNumbersAlert.setTitle("Позвонить в техподдержку");
        phoneNumbersAlert.setItems(separated, (DialogInterface dialog, int position) -> {
            if (getActivity() != null && getContext() != null) {
                if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel: " + separated[position]));
                    listener.startIntent(intent);
                }
            }
        });
        phoneNumbersAlert.show();
    }
}