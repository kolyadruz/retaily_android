package retaily.online.main.tabs.settings.viewmodel;

import android.hardware.usb.UsbRequest;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import retaily.online.models.response.login.UserInfo;
import retaily.online.repositories.MainRepository;

public class SettingsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SettingsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<UserInfo> getUserInfo(String code) {
        return MainRepository.getInstance().getUserInfo(code);
    }
}