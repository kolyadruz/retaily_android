package retaily.online.main.view;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import retaily.online.R;
import retaily.online.main.tabs.cart.shops.view.OrderAddedDialog;
import retaily.online.main.tabs.home.products.view.ProductDetailFragment;
import retaily.online.main.tabs.cart.shops.adapter.OnConfirmClickListener;
import retaily.online.main.tabs.home.products.adapter.OnDetailItemClickListener;
import retaily.online.main.adapter.OnFragmentsActionListener;
import retaily.online.main.tabs.home.suppliers.view.BannerDetailFragment;
import retaily.online.main.tabs.myorders.orders.adapter.OnOrderItemClick;
import retaily.online.main.tabs.home.products.adapter.OnProductDetailClickListener;
import retaily.online.models.response.getbanners.Banner;
import retaily.online.models.response.getorders.Order;
import retaily.online.models.response.getproducts.Product;
import retaily.online.models.response.getshops.Shop;
import retaily.online.main.navigation.TabRootFragment;
import retaily.online.utils.AppConstants;
import retaily.online.main.viewmodel.MainViewModel;
import retaily.online.main.tabs.cart.shops.view.ConfirmFragment;
import retaily.online.auth.login.view.LoginActivity;
import retaily.online.main.tabs.myorders.orders.view.OrderCancelDialog;
import retaily.online.utils.PrefKeys;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class MainActivity extends AppCompatActivity implements
        ViewPager.OnPageChangeListener,
        BottomNavigationView.OnNavigationItemReselectedListener,
        BottomNavigationView.OnNavigationItemSelectedListener,

        OnProductDetailClickListener,
        OnFragmentsActionListener,
        OnConfirmClickListener,
        OnDetailItemClickListener,
        OnOrderItemClick {

    BottomNavigationView bottomNav;

    Stack<Integer> backStack = new Stack<>();

    private final ArrayList<TabRootFragment> fragments = new ArrayList<>();

    private final List<Integer> indexToPage = Arrays.asList(R.id.suppliers, R.id.cart, R.id.orders, R.id.settings);

    ViewPager mainPager;

    ViewPagerAdapter pagerAdapter;

    MainViewModel mainViewModel;

    String token;

    OnConfirmClickListener shopsOnConfirmListener;
    OnDetailItemClickListener detailItemClickListener;
    OnOrderItemClick orderItemClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        token = getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE).getString(PrefKeys.TOKEN_KEY, "");

        mainViewModel = new ViewModelProvider(getViewModelStore(),
                new ViewModelProvider.AndroidViewModelFactory(getApplication()))
                .get(MainViewModel.class);

        fragments.add(TabRootFragment.newInstance(R.layout.content_suppliers, R.id.toolbar_suppliers, R.id.nav_host_suppliers));
        fragments.add(TabRootFragment.newInstance(R.layout.content_cart, R.id.toolbar_cart, R.id.nav_host_cart));
        fragments.add(TabRootFragment.newInstance(R.layout.content_orders, R.id.toolbar_orders, R.id.nav_host_orders));
        fragments.add(TabRootFragment.newInstance(R.layout.content_settings, R.id.toolbar_settings, R.id.nav_host_settings));

        mainPager = findViewById(R.id.main_pager);

        mainPager.addOnPageChangeListener(this);
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mainPager.setAdapter(pagerAdapter);
        mainPager.post(this::checkDeepLink);
        mainPager.setOffscreenPageLimit(fragments.size());

        bottomNav = findViewById(R.id.bottom_nav);
        bottomNav.setOnNavigationItemSelectedListener(this);
        bottomNav.setOnNavigationItemReselectedListener(this);

        bottomNav.setItemIconTintList(null);

        if (backStack.empty()) backStack.push(0);

        if (getIntent().getAction() != null) {
            if (getIntent().getAction().equals(AppConstants.SHOW_ORDERS_ACTION)) {
                setTab(2);
            }
        }

    }

    @Override
    public void onProductDetailInCartClick() {
        bottomNav.setSelectedItemId(R.id.cart_dest);
    }

    @Override
    public void showLoginPage(boolean isTokenExpired) {
        if (!isTokenExpired) {
            getSharedPreferences(AppConstants.PREF_NAME, MODE_PRIVATE).edit()
                    .putString(PrefKeys.LOGIN_KEY, "")
                    .putString(PrefKeys.PASS_KEY, "")
                    .putString(PrefKeys.PIN_KEY, "")
                    .apply();
        }

        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void showProductDetail(OnDetailItemClickListener listener, Product product, String supplierName, int itemPosition, boolean hasContract) {
        detailItemClickListener = listener;

        String productStr = new Gson().toJson(product);
        ProductDetailFragment productDetail = ProductDetailFragment.newInstance(productStr, supplierName, itemPosition, hasContract);
        productDetail.show(getSupportFragmentManager(), productDetail.getTag());
    }

    @Override
    public void showBannerDetail(Banner banner) {
        String bannerStr = new Gson().toJson(banner);
        BannerDetailFragment bannerDetailFragment = BannerDetailFragment.newInstance(bannerStr);
        bannerDetailFragment.show(getSupportFragmentManager(), bannerDetailFragment.getTag());
    }

    @Override
    public void onCartChangeBtnClicked(int position) {
        detailItemClickListener.onCartChangeBtnClicked(position);
    }

    @Override
    public void onInCartButtonClicked() {
        detailItemClickListener.onInCartButtonClicked();
    }

    @Override
    public void showCallForContractDialog() {
        detailItemClickListener.showCallForContractDialog();
    }

    @Override
    public void showConfirmDialog(OnConfirmClickListener listener, Shop shop, double summ, String supplierCode) {
        this.shopsOnConfirmListener = listener;
        String shopStr = new Gson().toJson(shop);
        ConfirmFragment confirmFragment = ConfirmFragment.newInstance(shopStr, summ, supplierCode);
        confirmFragment.show(getSupportFragmentManager(), confirmFragment.getTag());
    }

    @Override
    public void onConfirm(String supplierCode) {
        shopsOnConfirmListener.onConfirm(supplierCode);
    }

    @Override
    public void showOrderAddedDialog(String orderCode, String message) {
        OrderAddedDialog dialog = new OrderAddedDialog(orderCode, this, message);
        dialog.show(getSupportFragmentManager(), dialog.getTag());
    }

    @Override
    public void changeCartIcon(boolean isEmpty) {
        bottomNav.getMenu().findItem(R.id.cart).setIcon(isEmpty? ContextCompat.getDrawable(this, R.drawable.nav_cart_empty_icon_selector) : ContextCompat.getDrawable(this, R.drawable.nav_cart_full_icon_selector));
    }

    @Override
    public void setTab(int position) {
        setItem(position);
    }

    @Override
    public void startIntent(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showDeleteDialog(int orderNum, String orderCode, long orderBalance, OnOrderItemClick listener) {

        orderItemClick = listener;

        OrderCancelDialog dialog = OrderCancelDialog.newInstance(orderNum, orderCode, orderBalance);
        dialog.show(getSupportFragmentManager(), dialog.getTag());
    }

    @Override
    public void onClick(Order order) {
        orderItemClick.onClick(order);
    }

    @Override
    public void cancelOrder(int orderNum, String orderCode, long balance) {
        orderItemClick.cancelOrder(orderNum, orderCode, balance);
    }

    @Override
    public void onConfirmCancel(String orderCode, long balance) {
        orderItemClick.onConfirmCancel(orderCode, balance);
    }

    @Override
    public void showCallSupplierForContractDialog(String phones, boolean isAboutContract) {
        String[] separated = phones.split(",");

        AlertDialog.Builder phoneNumbersAlert = new AlertDialog.Builder(this);

        TextView textView = new TextView(this);
        textView.setTextColor(getResources().getColor(R.color.black));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20,20,20,20);
        textView.setLayoutParams(layoutParams);
        if (isAboutContract) {
            textView.setText("У вас не заключен договор с поставщиком\n\n Для заключения договора позвоните поставщику нажав по одному из номеров:");
        } else {
            textView.setText("Выберите номер телефона для звонка\n\n Будьте готовы назвать номер заказа оператору");
        }
        phoneNumbersAlert.setCustomTitle(textView);

        phoneNumbersAlert.setItems(separated, (DialogInterface dialog, int position) -> {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},1);
            } else {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel: " + separated[position]));
                startIntent(intent);
            }
        });

        phoneNumbersAlert.show();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int position = indexToPage.indexOf(item.getItemId());
        if (mainPager.getCurrentItem() != position) setItem(position);
        return true;
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {
        int position = indexToPage.indexOf(item.getItemId());
        TabRootFragment fragment = fragments.get(position);
        fragment.popToRoot();
    }

    @Override
    public void onBackPressed() {
        TabRootFragment fragment = fragments.get(mainPager.getCurrentItem());
        boolean hadNestedFragments = fragment.onBackPressed();

        if(!hadNestedFragments) {
            if(backStack.size() > 1) {
                backStack.pop();
                mainPager.setCurrentItem(backStack.peek());
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        Integer itemId = indexToPage.get(position);
        if (bottomNav.getSelectedItemId() != itemId) bottomNav.setSelectedItemId(itemId);
    }

    private void setItem(Integer position) {
        mainPager.setCurrentItem(position);
        backStack.push(position);
    }

    private void checkDeepLink() {
        for (int i = 0; i < fragments.size(); i++) {
            boolean hasDeepLink = fragments.get(i).handleDeepLink(getIntent());
            if (hasDeepLink) setItem(i);
        }
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(@NonNull FragmentManager fm) {
            super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NotNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}