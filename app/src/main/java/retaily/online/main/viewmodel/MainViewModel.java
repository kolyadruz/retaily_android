package retaily.online.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import retaily.online.models.request.CmdBody;
import retaily.online.models.response.getcategories.CategoryResponse;
import retaily.online.models.response.getsuppliers.SuppliersWithTags;
import retaily.online.repositories.MainRepository;
import retaily.online.service.Event;

public class MainViewModel extends AndroidViewModel {
    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public void sendFBtoken(String bearerToken, CmdBody cmdBody) {
        MainRepository.getInstance().sendFBtoken(bearerToken, cmdBody);
    }

    public LiveData<Event<SuppliersWithTags>> getSuppliersWithTags(String token, CmdBody cmdBody) {
        return MainRepository.getInstance().getSuppliers(token, cmdBody);
    }

    public LiveData<Event<CategoryResponse>> getCategories(String token, CmdBody cmdBody) {
        return MainRepository.getInstance().getCategories(token, cmdBody);
    }

}
