package retaily.online.models.local;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import retaily.online.models.response.TypedCartItem;
import retaily.online.models.response.getproducts.Product;

@Entity(tableName = "cart")
public class Cart extends TypedCartItem {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "cart_quantity")
    private long quantity;

    @Embedded
    private Product product;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Cart(Product product, long quantity) {
        this.quantity = quantity;
        this.product = product;
    }

    @Ignore
    public Cart(Product product) {
        this.product = product;
    }

    @Override
    public int getType() {
        return TYPE_PRODUCT;
    }
}
