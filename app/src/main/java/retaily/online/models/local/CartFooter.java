package retaily.online.models.local;

import retaily.online.models.response.TypedCartItem;

public class CartFooter extends TypedCartItem {

    private String supplierCode;
    private double total_cost;
    private long balance;

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public double getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(double total_cost) {
        this.total_cost = total_cost;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public CartFooter(String supplierCode, double total_cost, long balance) {
        this.supplierCode = supplierCode;
        this.total_cost = total_cost;
        this.balance = balance;
    }

    public CartFooter(SupplierWithCartsObj supplierWithCartsObj) {
        this.supplierCode = supplierWithCartsObj.getSupplier().getName();
        this.total_cost = supplierWithCartsObj.getTotal_cost();
        this.balance = supplierWithCartsObj.getCarts().get(0).getProduct().getBalance();
    }

    @Override
    public int getType() {
        return TYPE_FOOTER;
    }
}
