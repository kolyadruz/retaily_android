package retaily.online.models.local;

import androidx.room.Ignore;

import retaily.online.models.response.TypedCartItem;

public class CartHeader extends TypedCartItem {

    private String product_supplier;
    private Double total_cost;
    private long product_balance;
    @Ignore
    private String time;
    @Ignore
    private int num;
    @Ignore
    private String phone_support;
    @Ignore
    private String status;
    @Ignore
    private String supplierName;

    public String getProduct_supplier() {
        return product_supplier;
    }

    public void setProduct_supplier(String product_supplier) {
        this.product_supplier = product_supplier;
    }

    public Double getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(Double total_cost) {
        this.total_cost = total_cost;
    }

    public long getProduct_balance() {
        return product_balance;
    }

    public void setProduct_balance(long product_balance) {
        this.product_balance = product_balance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getPhone_support() {
        return phone_support;
    }

    public void setPhone_support(String phone_support) {
        this.phone_support = phone_support;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public CartHeader(String product_supplier, Double total_cost) {
        this.product_supplier = product_supplier;
        this.total_cost = total_cost;
    }

    @Ignore
    public CartHeader(String product_supplier, int num, Double total_cost, String phones, String status) {
        this.num = num;
        this.product_supplier = product_supplier;
        this.total_cost = total_cost;
        this.phone_support = phones;
        this.status = status;
    }

    @Ignore
    public CartHeader(SupplierWithCartsObj supplierWithCartsObj) {
        this.product_supplier = supplierWithCartsObj.getSupplier().getName();
        this.total_cost = supplierWithCartsObj.getTotal_cost();
        this.phone_support = supplierWithCartsObj.getSupplier().getPhone_support();
    }

    @Override
    public int getType() {
        return TYPE_HEADER;
    }
}
