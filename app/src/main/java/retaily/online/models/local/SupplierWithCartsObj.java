package retaily.online.models.local;

import java.util.List;

import retaily.online.models.response.getsuppliers.Supplier;

public class SupplierWithCartsObj {

    private Supplier supplier;
    private List<Cart> carts;
    private double total_cost;

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public List<Cart> getCarts() {
        return carts;
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
    }

    public double getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(double total_cost) {
        this.total_cost = total_cost;
    }

    public SupplierWithCartsObj(Supplier supplier, List<Cart> carts, double total_cost) {
        this.supplier = supplier;
        this.carts = carts;
        this.total_cost = total_cost;
    }
}
