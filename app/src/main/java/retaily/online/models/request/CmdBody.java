package retaily.online.models.request;

import java.util.List;

public class CmdBody {

    private String cmd;
    private String supplier;
    private String category;
    private String shop;
    private String payment;
    private String delivery;
    private String firebaseToken;
    private String order;
    private int rowCount;
    private int rowSkip;
    private List<ProductBody> products;
    private List<String> codes;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public List<ProductBody> getProducts() {
        return products;
    }

    public void setProducts(List<ProductBody> products) {
        this.products = products;
    }

    public String getFBtoken() {
        return firebaseToken;
    }

    public void setFBtoken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getRowCount() {
        return rowCount;
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    public int getRowSkip() {
        return rowSkip;
    }

    public void setRowSkip(int rowSkip) {
        this.rowSkip = rowSkip;
    }

    public List<String> getCodes() {
        return codes;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }

    public CmdBody(String cmd) {
        this.cmd = cmd;
    }

    public CmdBody(String cmd, String supplier) {
        this.cmd = cmd;
        this.supplier = supplier;
    }

    public CmdBody(String cmd, String supplier, String category) {
        this.cmd = cmd;
        this.supplier = supplier;
        this.category = category;
    }

    public CmdBody(String cmd, String supplier, String shop, List<ProductBody> products) {
        this.cmd = cmd;
        this.supplier = supplier;
        this.shop = shop;
        this.products = products;
    }
}
