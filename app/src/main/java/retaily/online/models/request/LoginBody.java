package retaily.online.models.request;

public class LoginBody {

    private String login;
    private String psw;
    private String type;
    private String model;

    public LoginBody(String login, String pass, String type, String model) {
        this.login = login;
        this.psw = pass;
        this.type = type;
        this.model = model;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return psw;
    }

    public void setPassword(String psw) {
        this.psw = psw;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public boolean isInputDataValid() {
        return getLogin().length() >= 1 && getPassword().length() > 1;
    }
}
