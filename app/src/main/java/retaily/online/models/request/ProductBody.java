package retaily.online.models.request;

public class ProductBody {

    private String product;
    private long quantity;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public ProductBody(String product, long quantity) {
        this.product = product;
        this.quantity = quantity;
    }
}
