package retaily.online.models.response;

public abstract class TypedCartItem {

    public static final int TYPE_HEADER = 0;
    public static final int TYPE_PRODUCT = 1;
    public static final int TYPE_FOOTER = 2;


    abstract public int getType();
}
