package retaily.online.models.response.checkproducts;

import java.util.ArrayList;
import java.util.List;

import retaily.online.models.response.getproducts.Product;

public class CheckProductsResponse {

    private ArrayList<Product> products;
    private ArrayList<Comment> comments;
    private Throwable error;
    private int response_code;
    private String message;

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CheckProductsResponse(CheckProductsResponse checkProductsResponse) {
        this.response_code = 200;
        this.products = checkProductsResponse.getProducts();
        this.comments = checkProductsResponse.getComments();
    }

    public CheckProductsResponse(Throwable error) {
        this.error = error;
        this.products = null;
        this.comments = null;
    }

    public CheckProductsResponse(int response_code, String message) {
        this.response_code = response_code;
        this.message = message;
        this.error = null;
    }
}
