package retaily.online.models.response.checkproducts;

import retaily.online.models.response.getproducts.Product;

public class Comment {

    private Product product;
    private String comment;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Comment(Product product, String comment) {
        this.product = product;
        this.comment = comment;
    }
}
