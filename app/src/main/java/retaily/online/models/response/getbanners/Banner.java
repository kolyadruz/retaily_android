package retaily.online.models.response.getbanners;

import java.util.ArrayList;

public class Banner {

    private String code;
    private String title;
    private String content;
    private long ord;
    private boolean visible;
    private String type;
    private String supplier;
    private String link;
    private String link_type;
    private String date_create;
    private ArrayList<String> images;
    private Throwable error;
    private int response_code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getOrd() {
        return ord;
    }

    public void setOrd(long ord) {
        this.ord = ord;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink_type() {
        return link_type;
    }

    public void setLink_type(String link_type) {
        this.link_type = link_type;
    }

    public String getDate_create() {
        return date_create;
    }

    public void setDate_create(String date_create) {
        this.date_create = date_create;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Banner(String code, String title, String content, long ord, boolean visible, String type, String supplier, String link, String link_type, String date_create, ArrayList<String> images) {
        this.code = code;
        this.title = title;
        this.content = content;
        this.ord = ord;
        this.visible = visible;
        this.type = type;
        this.supplier = supplier;
        this.link = link;
        this.link_type = link_type;
        this.date_create = date_create;
        this.images = images;
    }
}
