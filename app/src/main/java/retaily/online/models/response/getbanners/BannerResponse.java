package retaily.online.models.response.getbanners;

import java.util.ArrayList;
import java.util.List;

public class BannerResponse {

    private ArrayList<Banner> data = new ArrayList<>();
    private Throwable error;
    private int response_code;
    private String message;

    public ArrayList<Banner> getData() {
        return data;
    }

    public void setData(ArrayList<Banner> data) {
        this.data = data;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BannerResponse(List<Banner> banners) {
        this.response_code = 200;
        this.data.clear();
        if (banners != null) {
            this.data.addAll(banners);
        }
        this.error = null;
    }

    public BannerResponse(int response_code, String message) {
        this.response_code = response_code;
        this.message = message;
    }

    public BannerResponse(Throwable error) {
        this.error = error;
    }
}
