package retaily.online.models.response.getcategories;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import retaily.online.utils.StringsArrayListConverter;

import java.util.ArrayList;

@Entity(tableName = "categories")
public class Category {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "category_code")
    private String code;

    @ColumnInfo(name = "category_name")
    private String name;

    @ColumnInfo(name = "category_ord")
    private int ord;

    @ColumnInfo(name = "category_parent_code")
    private String parent_code;

    @ColumnInfo(name = "category_images")
    @TypeConverters(StringsArrayListConverter.class)
    private ArrayList<String> images;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    public String getParent_code() {
        return parent_code;
    }

    public void setParent_code(String parent_code) {
        this.parent_code = parent_code;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    @Ignore
    public Category(){}

    public Category(String code, String name, int ord, String parent_code, ArrayList<String> images) {
        this.code = code;
        this.name = name;
        this.ord = ord;
        this.parent_code = parent_code;
        this.images = images;
    }
}
