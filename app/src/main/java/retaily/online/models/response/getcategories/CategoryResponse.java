package retaily.online.models.response.getcategories;

import java.util.ArrayList;
import java.util.List;

public class CategoryResponse {

    ArrayList<Category> categories;
    private Throwable error;
    private int response_code;
    private String message;

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CategoryResponse(Throwable error) {
        this.error = error;
        categories = null;
    }

    public CategoryResponse(int response_code, String message) {
        this.response_code = response_code;
        this.message = message;
        categories = null;
    }

    public CategoryResponse(List<Category> categories) {
        response_code = 200;
        error = null;
        this.categories = (ArrayList<Category>) categories;
    }
}
