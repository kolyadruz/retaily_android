package retaily.online.models.response.getorders;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import java.util.ArrayList;

import retaily.online.models.response.objects.status.Status;

public class Order {
    private String code;
    private String status;
    private String payment;
    private double total_cost;
    private String supplier_name;
    private String supplier_phone_support;
    private String code_supplier;
    private String supplier;
    private int num;
    private String date_create;
    private String date_update;
    private String customer;
    private String trader;
    private String shop;
    private String shop_name;
    private ArrayList<OrderProduct> orders_products;
    private String message;

    private Status statusObj;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public double getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(double total_cost) {
        this.total_cost = total_cost;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getDate_create() {
        return date_create;
    }

    public void setDate_create(String date_create) {
        this.date_create = date_create;
    }

    public String getDate_update() {
        return date_update;
    }

    public void setDate_update(String date_update) {
        this.date_update = date_update;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getTrader() {
        return trader;
    }

    public void setTrader(String trader) {
        this.trader = trader;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public ArrayList<OrderProduct> getOrders_products() {
        return orders_products;
    }

    public void setOrders_products(ArrayList<OrderProduct> orders_products) {
        this.orders_products = orders_products;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSupplier_phone_support() {
        return supplier_phone_support;
    }

    public void setSupplier_phone_support(String supplier_phone_support) {
        this.supplier_phone_support = supplier_phone_support;
    }

    public String getCode_supplier() {
        return code_supplier;
    }

    public void setCode_supplier(String code_supplier) {
        this.code_supplier = code_supplier;
    }

    public Status getStatusObj() {
        return statusObj;
    }

    public void setStatusObj(Status statusObj) {
        this.statusObj = statusObj;
    }

    public static final DiffUtil.ItemCallback<Order> CALLBACK = new DiffUtil.ItemCallback<Order>() {
        @Override
        public boolean areItemsTheSame(@NonNull Order order, @NonNull Order t1) {
            return order.code.equals(t1.code);
        }

        @Override
        public boolean areContentsTheSame(@NonNull Order order, @NonNull Order t1) {
            return true;
        }
    };
}
