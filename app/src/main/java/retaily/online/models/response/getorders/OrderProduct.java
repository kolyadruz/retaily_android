package retaily.online.models.response.getorders;

import retaily.online.models.response.TypedCartItem;

import java.util.ArrayList;

public class OrderProduct extends TypedCartItem {

    private String code;
    private long quantity;
    private String product;
    private double price;
    private double total_cost;
    private String name;
    private int quantum;
    private String description_short;
    private ArrayList<String> images;
    private long balance;
    private int step;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(double total_cost) {
        this.total_cost = total_cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantum() {
        return quantum;
    }

    public void setQuantum(int quantum) {
        this.quantum = quantum;
    }

    public String getDescription_short() {
        return description_short;
    }

    public void setDescription_short(String description_short) {
        this.description_short = description_short;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public OrderProduct(String code, long quantity, String product, double price, double total_cost, String name, String description_short, ArrayList<String> images, long balance, int step) {
        this.code = code;
        this.quantity = quantity;
        this.product = product;
        this.price = price;
        this.total_cost = total_cost;
        this.name = name;
        this.description_short = description_short;
        this.images = images;
        this.balance = balance;
        this.step = step;
    }

    @Override
    public int getType() {
        return TYPE_PRODUCT;
    }
}
