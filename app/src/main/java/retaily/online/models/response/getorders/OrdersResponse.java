package retaily.online.models.response.getorders;

import java.util.ArrayList;
import java.util.List;

public class OrdersResponse {

    private ArrayList<Order> orders;
    private Throwable error;
    private int response_code;

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public OrdersResponse(List<Order> orders) {
        this.orders = (ArrayList<Order>) orders;
        this.response_code = 200;
        this.error = null;
    }

    public OrdersResponse(Throwable error) {
        this.error = error;
        this.orders = null;
    }

    public OrdersResponse(int response_code) {
        this.response_code = response_code;
        this.error = null;
        this.orders = null;
    }
}
