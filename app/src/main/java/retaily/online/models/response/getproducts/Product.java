package retaily.online.models.response.getproducts;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import retaily.online.utils.StringsArrayListConverter;

import java.util.ArrayList;

@Entity(tableName = "products")
public class Product {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "product_code")
    private String code;

    @ColumnInfo(name = "product_category")
    private String category;

    @ColumnInfo(name = "product_category_sub")
    private String category_sub;

    @ColumnInfo(name = "product_name")
    private String name;

    @ColumnInfo(name = "product_quantum")
    private int quantum;

    @ColumnInfo(name = "product_visible")
    private boolean visible;

    @ColumnInfo(name = "product_supplier")
    private String supplier;

    @ColumnInfo(name = "product_ord")
    private int ord;

    @ColumnInfo(name = "product_description")
    private String description;

    @ColumnInfo(name = "product_description_short")
    private String description_short;

    @ColumnInfo(name = "product_price")
    private double price;

    @ColumnInfo(name = "product_images")
    @TypeConverters(StringsArrayListConverter.class)
    private ArrayList<String> images;

    @ColumnInfo(name = "product_balance")
    private long balance;

    @ColumnInfo(name = "product_step")
    private int step;

    @NonNull
    public String getCode() {
        return code;
    }

    public void setCode(@NonNull String code) {
        this.code = code;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory_sub() {
        return category_sub;
    }

    public void setCategory_sub(String category_sub) {
        this.category_sub = category_sub;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantum() {
        return quantum;
    }

    public void setQuantum(int quantum) {
        this.quantum = quantum;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription_short() {
        return description_short;
    }

    public void setDescription_short(String description_short) {
        this.description_short = description_short;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    /*@Ignore
    public Product(int type) {
        this.type = type;
    }*/

    @Ignore
    public Product() {
    }

    public Product(@NonNull String code, String category, String category_sub, String name, int quantum, boolean visible, String supplier, int ord, String description, String description_short, double price, ArrayList<String> images,long balance,  int step) {
        this.code = code;
        this.category = category;
        this.category_sub = category_sub;
        this.name = name;
        this.quantum = quantum;
        this.visible = visible;
        this.supplier = supplier;
        this.ord = ord;
        this.description = description;
        this.description_short = description_short;
        this.price = price;
        this.images = images;
        this.balance = balance;
        this.step = step;
    }
}
