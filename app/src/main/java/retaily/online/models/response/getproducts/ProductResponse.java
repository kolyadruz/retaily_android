package retaily.online.models.response.getproducts;

import java.util.ArrayList;
import java.util.List;

public class ProductResponse {

    private ArrayList<Product> products;
    private Throwable error;
    private int response_code;
    private String message;

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProductResponse(List<Product> products) {
        this.response_code = 200;
        this.products = (ArrayList<Product>) products;
    }

    public ProductResponse(Throwable error) {
        this.error = error;
        this.products = null;
    }

    public ProductResponse(int response_code, String message) {
        this.response_code = response_code;
        this.message = message;
        this.error = null;
    }
}
