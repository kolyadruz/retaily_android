package retaily.online.models.response.getshops;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "shops")
public class Shop {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "shop_code")
    private String code;

    @ColumnInfo(name = "shop_name")
    private String name;

    @ColumnInfo(name = "shop_ord")
    private int ord;

    @ColumnInfo(name = "shop_visible")
    private boolean visible;

    @ColumnInfo(name = "shop_parent_code")
    private String parent_code;

    @ColumnInfo(name = "shop_country")
    private String country;

    @ColumnInfo(name = "shop_region")
    private String region;

    @ColumnInfo(name = "shop_city")
    private String city;

    @ColumnInfo(name = "shop_street")
    private String street;

    @ColumnInfo(name = "shop_house")
    private String house;

    @ColumnInfo(name = "shop_status_supplier")
    private boolean status_supplier;

    @ColumnInfo(name = "shop_customer")
    private String customer;

    @ColumnInfo(name = "shop_price")
    private String price;

    @NonNull
    public String getCode() {
        return code;
    }

    public void setCode(@NonNull String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getParent_code() {
        return parent_code;
    }

    public void setParent_code(String parent_code) {
        this.parent_code = parent_code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public boolean isStatus_supplier() {
        return status_supplier;
    }

    public void setStatus_supplier(boolean status_supplier) {
        this.status_supplier = status_supplier;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Shop(@NonNull String code, String name, int ord, boolean visible, String parent_code, String country, String region, String city, String street, String house, boolean status_supplier, String customer, String price) {
        this.code = code;
        this.name = name;
        this.ord = ord;
        this.visible = visible;
        this.parent_code = parent_code;
        this.country = country;
        this.region = region;
        this.city = city;
        this.street = street;
        this.house = house;
        this.status_supplier = status_supplier;
        this.customer = customer;
        this.price = price;
    }
}
