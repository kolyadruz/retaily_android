package retaily.online.models.response.getshops;

import java.util.ArrayList;
import java.util.List;

public class ShopResponse {

    private ArrayList<Shop> shops;
    private Throwable error;
    private int response_code;
    private String message;

    public ArrayList<Shop> getShops() {
        return shops;
    }

    public void setShops(ArrayList<Shop> shops) {
        this.shops = shops;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ShopResponse(List<Shop> shops) {
        this.response_code = 200;
        this.shops = (ArrayList<Shop>) shops;
    }

    public ShopResponse(Throwable error) {
        this.error = error;
        this.shops = null;
    }

    public ShopResponse(int response_code, String message) {
        this.response_code = response_code;
        this.message = message;
        this.error = null;
    }
}
