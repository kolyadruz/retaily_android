package retaily.online.models.response.getstories;

import java.util.ArrayList;
import java.util.List;

import retaily.online.models.response.getbanners.Banner;

public class StoriesResponse {

    private ArrayList<Story> data = new ArrayList<>();
    private Throwable error;
    private int response_code;
    private String message;

    public ArrayList<Story> getData() {
        return data;
    }

    public void setData(ArrayList<Story> data) {
        this.data = data;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StoriesResponse(List<Story> stories) {
        this.response_code = 200;
        this.data.clear();
        if (stories != null) {
            this.data.addAll(stories);
        }
        this.error = null;
    }

    public StoriesResponse(int response_code, String message) {
        this.response_code = response_code;
        this.message = message;
    }

    public StoriesResponse(Throwable error) {
        this.error = error;
    }

}
