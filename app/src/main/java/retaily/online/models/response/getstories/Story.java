package retaily.online.models.response.getstories;

import java.util.List;

public class Story {

    private String code;
    private String title;
    private String media;
    private String media_type;
    private long ord;
    private boolean visible;
    private String supplier;
    private String link;
    private String link_type;
    private String date_create;
    private String bg_color;
    private String category;
    private String product;
    private boolean is_deleted;
    private String date_deleted;
    private List<String> images;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public long getOrd() {
        return ord;
    }

    public void setOrd(long ord) {
        this.ord = ord;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink_type() {
        return link_type;
    }

    public void setLink_type(String link_type) {
        this.link_type = link_type;
    }

    public String getDate_create() {
        return date_create;
    }

    public void setDate_create(String date_create) {
        this.date_create = date_create;
    }

    public String getBg_color() {
        return bg_color;
    }

    public void setBg_color(String bg_color) {
        this.bg_color = bg_color;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public boolean isIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getDate_deleted() {
        return date_deleted;
    }

    public void setDate_deleted(String date_deleted) {
        this.date_deleted = date_deleted;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Story(String code, String title, String media, String media_type, long ord, boolean visible, String supplier, String link, String link_type, String date_create, String bg_color, String category, String product, boolean is_deleted, String date_deleted, List<String> images) {
        this.code = code;
        this.title = title;
        this.media = media;
        this.media_type = media_type;
        this.ord = ord;
        this.visible = visible;
        this.supplier = supplier;
        this.link = link;
        this.link_type = link_type;
        this.date_create = date_create;
        this.bg_color = bg_color;
        this.category = category;
        this.product = product;
        this.is_deleted = is_deleted;
        this.date_deleted = date_deleted;
        this.images = images;
    }
}
