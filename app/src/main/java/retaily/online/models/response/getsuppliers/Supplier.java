package retaily.online.models.response.getsuppliers;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import retaily.online.utils.StringsArrayListConverter;

import java.util.ArrayList;

@Entity(tableName = "suppliers")
public class Supplier {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "supplier_code")
    private String code;

    @ColumnInfo(name = "supplier_name")
    private String name;

    @ColumnInfo(name = "supplier_ord")
    private int ord;

    @ColumnInfo(name = "supplier_tags")
    @TypeConverters(StringsArrayListConverter.class)
    private ArrayList<String> tags;

    @ColumnInfo(name = "supplier_has_contact")
    private boolean hasContract;

    @ColumnInfo(name = "supplier_phone_support")
    private String phone_support;

    @ColumnInfo(name = "supplier_images")
    @TypeConverters(StringsArrayListConverter.class)
    private ArrayList<String> images;

    @ColumnInfo(name = "supplier_type")
    private String type;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public boolean isHasContract() {
        return hasContract;
    }

    public void setHasContract(boolean hasContract) {
        this.hasContract = hasContract;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public String getPhone_support() {
        return phone_support;
    }

    public void setPhone_support(String phone_support) {
        this.phone_support = phone_support;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Supplier(@NonNull String code, String name, int ord, ArrayList<String> tags, boolean hasContract, ArrayList<String> images, String phone_support) {
        this.code = code;
        this.name = name;
        this.ord = ord;
        this.tags = tags;
        this.hasContract = hasContract;
        this.images = images;
        this.phone_support = phone_support;
    }
}
