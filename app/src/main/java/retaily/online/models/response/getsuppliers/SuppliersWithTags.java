package retaily.online.models.response.getsuppliers;

import java.util.ArrayList;

public class SuppliersWithTags {

    private ArrayList<Tag> tags;
    private ArrayList<Supplier> suppliers;
    private Throwable error;
    private int response_code;
    private String message;

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }

    public ArrayList<Supplier> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(ArrayList<Supplier> suppliers) {
        this.suppliers = suppliers;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SuppliersWithTags(SuppliersWithTags suppliersWithTags) {
        this.tags = suppliersWithTags.tags;
        this.suppliers = suppliersWithTags.suppliers;
        this.error = null;
        this.response_code = 200;
    }

    public SuppliersWithTags(Throwable error) {
        this.error = error;
        this.tags = null;
        this.suppliers = null;
    }

    public SuppliersWithTags(int response_code, String message) {
        this.response_code = response_code;
        this.message = message;
        this.error = null;
        this.tags = null;
        this.suppliers = null;
    }
}
