package retaily.online.models.response.getsuppliers;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "suppliers_tags")
public class Tag {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "tag_code")
    private String code;

    @ColumnInfo(name = "tag_name")
    private String name;

    @ColumnInfo(name = "tag_ord")
    private int ord;

    @ColumnInfo(name = "tag_visible")
    private boolean visible;

    @ColumnInfo(name = "tag_parent_code")
    private String parent_code;

    @NonNull
    public String getCode() {
        return code;
    }

    public void setCode(@NonNull String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getParent_code() {
        return parent_code;
    }

    public void setParent_code(String parent_code) {
        this.parent_code = parent_code;
    }

    public Tag(@NonNull String code, String name, int ord, boolean visible, String parent_code) {
        this.code = code;
        this.name = name;
        this.ord = ord;
        this.visible = visible;
        this.parent_code = parent_code;
    }
}
