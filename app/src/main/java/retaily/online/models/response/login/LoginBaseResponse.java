package retaily.online.models.response.login;

import androidx.room.Ignore;

import com.google.gson.annotations.SerializedName;

public class LoginBaseResponse {

    @SerializedName("token")
    private String token;

    @SerializedName("user")
    private UserInfo userInfo;

    @Ignore
    private Throwable error;

    @Ignore
    private int response_code;

    @Ignore
    private String message;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginBaseResponse(String token, UserInfo userInfo) {
        this.token = token;
        this.userInfo = userInfo;
        this.error = null;
        this.response_code = 200;
    }

    public LoginBaseResponse(LoginBaseResponse loginBaseResponse) {
        this.token = loginBaseResponse.token;
        this.userInfo = loginBaseResponse.userInfo;
        this.response_code = 200;
        this.error = null;
    }

    public LoginBaseResponse(Throwable error) {
        this.error = error;
        this.response_code = 0;
        this.token = null;
        this.userInfo = null;
    }

    public LoginBaseResponse(int response_code, String message) {
        this.response_code = response_code;
        this.message = message;
        this.error = null;
        this.token = null;
        this.userInfo = null;
    }
}
