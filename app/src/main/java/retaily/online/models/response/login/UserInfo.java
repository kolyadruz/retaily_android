package retaily.online.models.response.login;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "userinfo")
public class UserInfo {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "user_code")
    @SerializedName("code")
    public String code;

    @ColumnInfo(name = "user_login")
    @SerializedName("login")
    public String login;

    @ColumnInfo(name = "user_name")
    @SerializedName("name")
    public String name;

    @ColumnInfo(name = "user_role")
    @SerializedName("role")
    public String role;

    @ColumnInfo(name = "user_email")
    @SerializedName("email")
    public String email;

    @ColumnInfo(name = "user_first_auth")
    @SerializedName("first_auth")
    public String first_auth;

}
