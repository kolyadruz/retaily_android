package retaily.online.models.response.objects.status;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "statuses")
public class Status {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "status_id")
    private long id;

    @ColumnInfo(name = "status_code")
    private String code;

    @ColumnInfo(name = "status_name")
    private String name;

    @ColumnInfo(name = "status_parent_code")
    private String parent_code;

    @ColumnInfo(name = "status_ord")
    private int ord;

    @ColumnInfo(name = "status_visible")
    private boolean visible;

    @ColumnInfo(name = "status_color")
    private String color;

    @ColumnInfo(name = "status_is_cancelable")
    private boolean is_cancelable;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParent_code() {
        return parent_code;
    }

    public void setParent_code(String parent_code) {
        this.parent_code = parent_code;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isIs_cancelable() {
        return is_cancelable;
    }

    public void setIs_cancelable(boolean is_cancelable) {
        this.is_cancelable = is_cancelable;
    }

    public Status(long id, String code, String name, String parent_code, int ord, boolean visible, String color, boolean is_cancelable) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.parent_code = parent_code;
        this.ord = ord;
        this.visible = visible;
        this.color = color;
        this.is_cancelable = is_cancelable;
    }
}
