package retaily.online.models.response.objects.status;

import java.util.ArrayList;

public class StatusResponse {

    private ArrayList<Status> data;
    private Throwable error;
    private int response_code;
    private String message;

    public ArrayList<Status> getStatuses() {
        return data;
    }

    public void setStatuses(ArrayList<Status> statuses) {
        this.data = statuses;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public ArrayList<Status> getData() {
        return data;
    }

    public void setData(ArrayList<Status> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StatusResponse(StatusResponse statusResponse) {
        this.response_code = 200;
        this.data = statusResponse.getStatuses();
        this.error = null;
    }

    public StatusResponse(Throwable error) {
        this.error = error;
        this.data = null;
    }

    public StatusResponse(int response_code, String message) {
        this.response_code = response_code;
        this.message = message;
        this.error = null;
        this.data = null;
    }
}
