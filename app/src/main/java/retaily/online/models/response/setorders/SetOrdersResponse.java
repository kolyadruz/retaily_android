package retaily.online.models.response.setorders;

import retaily.online.models.response.getorders.Order;

public class SetOrdersResponse {

    private Order order;
    private Throwable error;
    private int response_code;
    private String message;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SetOrdersResponse(Order order) {
        this.order = order;
        this.response_code = 200;
    }

    public SetOrdersResponse(Throwable error) {
        this.error = error;
        this.order = null;
    }

    public SetOrdersResponse(int response_code, String message) {
        this.response_code = response_code;
        this.message = message;
        this.error = null;
        this.order = null;
    }
}
