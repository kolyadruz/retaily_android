package retaily.online.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.datatransport.backend.cct.BuildConfig;

import org.jetbrains.annotations.NotNull;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retaily.online.models.local.Cart;
import retaily.online.models.local.CartHeader;
import retaily.online.models.request.CmdBody;
import retaily.online.models.request.LoginBody;
import retaily.online.models.request.ProductBody;
import retaily.online.models.response.checkproducts.CheckProductsResponse;
import retaily.online.models.response.getbanners.Banner;
import retaily.online.models.response.getbanners.BannerResponse;
import retaily.online.models.response.getcategories.Category;
import retaily.online.models.response.getcategories.CategoryResponse;
import retaily.online.models.response.getorders.Order;
import retaily.online.models.response.getproducts.Product;
import retaily.online.models.response.getproducts.ProductResponse;
import retaily.online.models.response.getshops.Shop;
import retaily.online.models.response.getshops.ShopResponse;
import retaily.online.models.response.getstories.StoriesResponse;
import retaily.online.models.response.getstories.Story;
import retaily.online.models.response.getsuppliers.Supplier;
import retaily.online.models.response.getsuppliers.SuppliersWithTags;
import retaily.online.models.response.login.LoginBaseResponse;
import retaily.online.models.response.login.UserInfo;
import retaily.online.models.response.objects.status.Status;
import retaily.online.models.response.objects.status.StatusResponse;
import retaily.online.models.response.setorders.SetOrdersResponse;
import retaily.online.service.Event;
import retaily.online.service.IYakitProService;
import retaily.online.utils.AppConstants;
import retaily.online.auth.login.view.LoginActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainRepository {

    private static MainRepository mainRepository;

    private static IYakitProService iYakitProService;

    final MutableLiveData<Event<LoginBaseResponse>> login = new MutableLiveData<>();
    final MutableLiveData<Event<SuppliersWithTags>> suppliers = new MutableLiveData<>();
    final MutableLiveData<Event<CategoryResponse>> categoryResponse = new MutableLiveData<>();
    final MutableLiveData<Event<ProductResponse>> productsResponse = new MutableLiveData<>();
    final MutableLiveData<Event<CheckProductsResponse>> checkProductsResponse = new MutableLiveData<>();
    final MutableLiveData<ShopResponse> shopResponse = new MutableLiveData<>();
    List<Order> ordersResponse = new ArrayList<>();

    final MutableLiveData<Event<SetOrdersResponse>> setOrderResponse = new MutableLiveData<>();
    final MutableLiveData<Event<SetOrdersResponse>> setOrderBalanceResponse = new MutableLiveData<>();
    final MutableLiveData<Event<SetOrdersResponse>> repeatOrderResponse = new MutableLiveData<>();
    final MutableLiveData<Event<SetOrdersResponse>> repeatOrderBalanceResponse = new MutableLiveData<>();

    final MutableLiveData<Event<StatusResponse>> statusesResponse = new MutableLiveData<>();
    final MutableLiveData<List<Long>> statusesIds = new MutableLiveData<>();
    final MutableLiveData<List<Long>> cartsIds = new MutableLiveData<>();
    final MutableLiveData<Event<String>> stringEvent = new MutableLiveData<>();
    final MutableLiveData<String> string = new MutableLiveData<>();
    final MutableLiveData<BannerResponse> banners = new MutableLiveData<>();
    final MutableLiveData<StoriesResponse> stories = new MutableLiveData<>();

    public MainRepository() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(chain -> {
            String version = BuildConfig.VERSION_NAME;
            //version = version.replace(".", ",");
            //String[] separated = version.split(",");

            Request request = chain.request().newBuilder().addHeader("Version", version).addHeader("Content-Type", "application/json").build();
            return chain.proceed(request);
        });

        iYakitProService = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(AppConstants.SERVICE_URL)
                .client(httpClient.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(IYakitProService.class);
    }

    public static MainRepository getInstance() {
        if (mainRepository == null) {
            synchronized (MainRepository.class) {
                mainRepository = new MainRepository();
            }
        }
        return mainRepository;
    }

    public static IYakitProService getService() {
        if (iYakitProService != null) {
            return iYakitProService;
        } else {
            return null;
        }
    }

    private String getCustomErrorMessage(Response response) {
        BufferedReader reader;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public MutableLiveData<Event<LoginBaseResponse>> login(LoginBody loginBody) {
        iYakitProService.login(loginBody)
                .enqueue(new Callback<LoginBaseResponse>() {
                    @Override
                    public void onResponse(@NotNull Call<LoginBaseResponse> call, @NotNull Response<LoginBaseResponse> response) {
                        if (response.isSuccessful()) {
                            login.postValue(new Event<>(new LoginBaseResponse(response.body())));
                        } else {
                            login.postValue(new Event<>(new LoginBaseResponse(response.code(), getCustomErrorMessage(response))));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<LoginBaseResponse> call, @NotNull Throwable t) {
                        login.postValue(new Event<>(new LoginBaseResponse(t)));
                    }
                });

        return login;
    }

    public LiveData<Event<SuppliersWithTags>> getSuppliers(String bearerToken, CmdBody cmdBody) {
        iYakitProService.getSuppliers("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<SuppliersWithTags>() {
                    @Override
                    public void onResponse(@NotNull Call<SuppliersWithTags> call, @NotNull Response<SuppliersWithTags> response) {
                        if (response.isSuccessful()) {
                            suppliers.postValue(new Event<>(new SuppliersWithTags(response.body())));
                        } else {
                            suppliers.postValue(new Event<>(new SuppliersWithTags(response.code(), getCustomErrorMessage(response))));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call call, @NotNull Throwable t) {
                        suppliers.postValue(new Event<>(new SuppliersWithTags(t)));
                    }
                });
        return suppliers;
    }

    public LiveData<Event<CategoryResponse>> getCategories(String bearerToken, CmdBody cmdBody) {
        iYakitProService.getCategories("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<List<Category>>() {
                    @Override
                    public void onResponse(@NotNull Call<List<Category>> call, @NotNull Response<List<Category>> response) {
                        if (response.isSuccessful()) {
                            categoryResponse.postValue(new Event<>(new CategoryResponse(response.body())));
                        } else {
                            categoryResponse.postValue(new Event<>(new CategoryResponse(response.code(), getCustomErrorMessage(response))));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<List<Category>> call, @NotNull Throwable t) {
                        categoryResponse.postValue(new Event<>(new CategoryResponse(t)));
                    }
                });

        return categoryResponse;
    }

    public LiveData<Event<ProductResponse>> getProducts(String bearerToken, CmdBody cmdBody) {
        iYakitProService.getProducts("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<List<Product>>() {
                    @Override
                    public void onResponse(@NotNull Call<List<Product>> call, @NotNull Response<List<Product>> response) {
                        if (response.isSuccessful()) {
                            productsResponse.postValue(new Event<>(new ProductResponse(response.body())));
                        } else {
                            productsResponse.postValue(new Event<>(new ProductResponse(response.code(), getCustomErrorMessage(response))));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<List<Product>> call, @NotNull Throwable t) {
                        productsResponse.postValue(new Event<>(new ProductResponse(t)));
                    }
                });

        return productsResponse;
    }

    public LiveData<Event<CheckProductsResponse>> checkProducts(String bearerToken, CmdBody cmdBody) {
        iYakitProService.checkProducts("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<CheckProductsResponse>() {
                    @Override
                    public void onResponse(@NotNull Call<CheckProductsResponse> call, @NotNull Response<CheckProductsResponse> response) {
                        if (response.isSuccessful()) {
                            checkProductsResponse.postValue(new Event<>(new CheckProductsResponse(response.body())));
                        } else {
                            checkProductsResponse.postValue(new Event<>(new CheckProductsResponse(response.code(), getCustomErrorMessage(response))));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<CheckProductsResponse> call, @NotNull Throwable t) {
                        checkProductsResponse.postValue(new Event<>(new CheckProductsResponse(t)));
                    }
                });

        return checkProductsResponse;
    }

    public LiveData<ShopResponse> getShops(String bearerToken, CmdBody cmdBody) {
        iYakitProService.getShops("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<List<Shop>>() {
                    @Override
                    public void onResponse(@NotNull Call<List<Shop>> call, @NotNull Response<List<Shop>> response) {
                        if (response.isSuccessful()) {
                            shopResponse.postValue(new ShopResponse(response.body()));
                        } else {
                            shopResponse.postValue(new ShopResponse(response.code(), getCustomErrorMessage(response)));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<List<Shop>> call, @NotNull Throwable t) {
                        shopResponse.postValue(new ShopResponse(t));
                    }
                });

        return shopResponse;
    }

    public LiveData<Event<SetOrdersResponse>> setOrders(String bearerToken, CmdBody cmdBody) {
        iYakitProService.setOrders("Bearer " + bearerToken, cmdBody)
            .enqueue(new Callback<Order>() {
                @Override
                public void onResponse(@NotNull Call<Order> call, @NotNull Response<Order> response) {
                    if (response.isSuccessful()) {
                        setOrderResponse.postValue(new Event<>(new SetOrdersResponse(response.body())));
                    } else {
                        setOrderResponse.postValue(new Event<>(new SetOrdersResponse(response.code(), getCustomErrorMessage(response))));
                    }
                }

                @Override
                public void onFailure(@NotNull Call<Order> call, @NotNull Throwable t) {
                    setOrderResponse.postValue(new Event<>(new SetOrdersResponse(t)));
                }
            });
        return setOrderResponse;
    }

    public LiveData<Event<SetOrdersResponse>> repeatOrder(String bearerToken, CmdBody cmdBody) {
        iYakitProService.repeatOrder("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<Order>() {
                    @Override
                    public void onResponse(@NotNull Call<Order> call, @NotNull Response<Order> response) {
                        if (response.isSuccessful()) {
                            repeatOrderResponse.postValue(new Event<>(new SetOrdersResponse(response.body())));
                        } else {
                            repeatOrderResponse.postValue(new Event<>(new SetOrdersResponse(response.code(), getCustomErrorMessage(response))));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<Order> call, @NotNull Throwable t) {
                        repeatOrderResponse.postValue(new Event<>(new SetOrdersResponse(t)));
                    }
                });
        return repeatOrderResponse;
    }

    public LiveData<Event<String>> cancelOrder(String bearerToken, CmdBody cmdBody) {
        iYakitProService.cancelOrder("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
                        if (response.isSuccessful()) {
                            stringEvent.postValue(new Event<>(response.body()));
                        } else {
                            stringEvent.postValue(new Event<>(getCustomErrorMessage(response)));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                        stringEvent.postValue(new Event<>(""));
                    }
                });
        return stringEvent;
    }

    public LiveData<Event<SetOrdersResponse>> setOrdersBalance(String bearerToken, CmdBody cmdBody) {
        iYakitProService.setOrdersBalance("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<Order>() {
                    @Override
                    public void onResponse(@NotNull Call<Order> call, @NotNull Response<Order> response) {
                        if (response.isSuccessful()) {
                            setOrderBalanceResponse.postValue(new Event<>(new SetOrdersResponse(response.body())));
                        } else {
                            setOrderBalanceResponse.postValue(new Event<>(new SetOrdersResponse(response.code(), getCustomErrorMessage(response))));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<Order> call, @NotNull Throwable t) {
                        setOrderBalanceResponse.postValue(new Event<>(new SetOrdersResponse(t)));
                    }
                });
        return setOrderBalanceResponse;
    }

    public LiveData<Event<SetOrdersResponse>> repeatOrderBalance(String bearerToken, CmdBody cmdBody) {
        iYakitProService.repeatOrderBalance("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<Order>() {
                    @Override
                    public void onResponse(@NotNull Call<Order> call, @NotNull Response<Order> response) {
                        if (response.isSuccessful()) {
                            repeatOrderBalanceResponse.postValue(new Event<>(new SetOrdersResponse(response.body())));
                        } else {
                            repeatOrderBalanceResponse.postValue(new Event<>(new SetOrdersResponse(response.code(), getCustomErrorMessage(response))));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<Order> call, @NotNull Throwable t) {
                        repeatOrderBalanceResponse.postValue(new Event<>(new SetOrdersResponse(t)));
                    }
                });
        return repeatOrderBalanceResponse;
    }

    public LiveData<Event<String>> cancelOrderBalance(String bearerToken, CmdBody cmdBody) {
        iYakitProService.cancelOrderBalance("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
                        if (response.isSuccessful()) {
                            stringEvent.postValue(new Event<>(response.body()));
                        } else {
                            stringEvent.postValue(new Event<>(""));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                        stringEvent.postValue(new Event<>(""));
                    }
                });
        return stringEvent;
    }

    public List<Order> getOrders(String bearerToken, CmdBody cmdBody) {
        iYakitProService.getOrders("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<List<Order>>() {
                    @Override
                    public void onResponse(@NotNull Call<List<Order>> call, @NotNull Response<List<Order>> response) {
                        if (response.isSuccessful()) {
                            ordersResponse = response.body();
                        } else {
                            ordersResponse = null;
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<List<Order>> call, @NotNull Throwable t) {
                        ordersResponse = null;
                    }
                });
        return ordersResponse;
    }

    public LiveData<Event<StatusResponse>> getStatuses(String bearerToken) {
        iYakitProService.getStatuses("Bearer " + bearerToken)
                .enqueue(new Callback<StatusResponse>() {
                    @Override
                    public void onResponse(@NotNull Call<StatusResponse> call, @NotNull Response<StatusResponse> response) {
                        if (response.isSuccessful()) {
                            statusesResponse.postValue(new Event<>(new StatusResponse(response.body())));
                        } else {
                            statusesResponse.postValue(new Event<>(new StatusResponse(response.code(), getCustomErrorMessage(response))));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<StatusResponse> call, @NotNull Throwable t) {
                        statusesResponse.postValue(new Event<>(new StatusResponse(t)));
                    }
                });
        return statusesResponse;
    }

    public LiveData<String> sendFBtoken(String bearerToken, CmdBody cmdBody) {
        iYakitProService.sendFBtoken("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
                        if (response.isSuccessful()) {
                            string.postValue(response.body());
                        } else {
                            string.postValue("");
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                        string.postValue("");
                    }
                });
        return string;
    }

    public LiveData<BannerResponse> getBanners(String bearerToken, CmdBody cmdBody) {
        iYakitProService.getBanners("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<List<Banner>>() {
                    @Override
                    public void onResponse(@NotNull Call<List<Banner>> call, @NotNull Response<List<Banner>> response) {
                        if (response.isSuccessful()) {
                            banners.postValue(new BannerResponse(response.body()));
                        } else {
                            banners.postValue(new BannerResponse(response.code(), response.body().toString()));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<List<Banner>> call, @NotNull Throwable t) {
                        banners.postValue(new BannerResponse(t));
                    }
                });
        return banners;
    }

    public LiveData<StoriesResponse> getStories(String bearerToken, CmdBody cmdBody) {
        iYakitProService.getStories("Bearer " + bearerToken, cmdBody)
                .enqueue(new Callback<List<Story>>() {
                    @Override
                    public void onResponse(@NotNull Call<List<Story>> call, @NotNull Response<List<Story>> response) {
                        if (response.isSuccessful()) {
                            stories.postValue(new StoriesResponse(response.body()));
                        } else {
                            stories.postValue(new StoriesResponse(response.code(), response.body().toString()));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<List<Story>> call, @NotNull Throwable t) {
                        stories.postValue(new StoriesResponse(t));
                    }
                });
        return stories;
    }

    /** USER_INFO **/
    public void insert(UserInfo userInfo) {
        LoginActivity.appDataBase.myDao().insertuserinfo(userInfo);
    }

    public LiveData<UserInfo> getUserInfo(String code) {
        return LoginActivity.appDataBase.myDao().getuserinfo(code);
    }

    /** CART **/
    public long checkInCart(Product product) {
        return LoginActivity.appDataBase.myDao().checkInCart(product.getSupplier(), product.getCode());
    }

    public LiveData<Long> checkDetailInCart(Product product) {
        return LoginActivity.appDataBase.myDao().checkDetailInCart(product.getSupplier(), product.getCode());
    }

    public Cart getCart(Product product) {
        return LoginActivity.appDataBase.myDao().getCart(product.getSupplier(), product.getCode());
    }

    public long addToCart(Cart cart) {
        return LoginActivity.appDataBase.myDao().addToCart(cart);
    }

    public LiveData<List<Long>> addToCart(List<Cart> carts) {
        cartsIds.postValue(LoginActivity.appDataBase.myDao().addToCart(carts));
        return cartsIds;
    }

    public void deleteFromCart(Cart cart) {
        LoginActivity.appDataBase.myDao().deleteCart(cart);
    }

    public void deleteSupplierFromCart(String supplierCode) {
        LoginActivity.appDataBase.myDao().deleteCart(supplierCode);
    }

    public LiveData<Double> getAllProductsSumInCart() {
        return LoginActivity.appDataBase.myDao().getAllProductsSumInCart();
    }

    public LiveData<List<CartHeader>> getCartHeaders() {
        return LoginActivity.appDataBase.myDao().getCartHeaders();
    }

    public List<Cart> getCartsBySupplier(String supplierCode) {
        return LoginActivity.appDataBase.myDao().getProductsBySupplier(supplierCode);
    }

    public void clearAllFromCart() {
        LoginActivity.appDataBase.myDao().clearAllFromCart();
    }

    /** STATUSES **/
    public LiveData<List<Long>> addStatuses(ArrayList<Status> statuses) {
        List<Long> ids = LoginActivity.appDataBase.myDao().insertStatuses(statuses);
        statusesIds.postValue(ids);
        return statusesIds;
    }

    public LiveData<List<Status>> getStatuses() {
        return LoginActivity.appDataBase.myDao().getStatuses();
    }

    /** SUPPLIERS **/
    public LiveData<List<Long>> addSuppliers(ArrayList<Supplier> suppliers) {
        List<Long> ids = LoginActivity.appDataBase.myDao().insertsuppliers(suppliers);
        statusesIds.postValue(ids);
        return statusesIds;
    }

    public Supplier getSupplier(String supplierCode) {
        return LoginActivity.appDataBase.myDao().getsupplier(supplierCode);
    }

    public List<String> getSuppliersInCart() {
        return LoginActivity.appDataBase.myDao().getSuppliersInCart();
    }

    public List<ProductBody> getProductCodesBySupplier(String supplierCode) {
        return LoginActivity.appDataBase.myDao().getProductCodesBySupplier(supplierCode);
    }

    /** CATEGORIES **/
    public LiveData<List<Long>> addCategories(ArrayList<Category> categories) {
        List<Long> ids = LoginActivity.appDataBase.myDao().insertcategories(categories);
        statusesIds.postValue(ids);
        return statusesIds;
    }

    public Category getCategory(String categoryCode) {
        return LoginActivity.appDataBase.myDao().getcategory(categoryCode);
    }

}
