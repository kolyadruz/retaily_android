package retaily.online.service;

import retaily.online.models.request.CmdBody;
import retaily.online.models.request.LoginBody;
import retaily.online.models.response.checkproducts.CheckProductsResponse;
import retaily.online.models.response.getbanners.Banner;
import retaily.online.models.response.getcategories.Category;
import retaily.online.models.response.getorders.Order;
import retaily.online.models.response.getshops.Shop;
import retaily.online.models.response.getstories.Story;
import retaily.online.models.response.login.LoginBaseResponse;
import retaily.online.models.response.getproducts.Product;
import retaily.online.models.response.getsuppliers.SuppliersWithTags;
import retaily.online.models.response.objects.status.StatusResponse;
import retaily.online.utils.AppConstants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface IYakitProService {

    @POST(AppConstants.AUTH)
    Call<LoginBaseResponse> login(@Body LoginBody loginBody);

    @POST(AppConstants.QUERY)
    Call<SuppliersWithTags> getSuppliers(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY)
    Call<List<Category>> getCategories(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY)
    Call<List<Product>> getProducts(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY)
    Call<CheckProductsResponse> checkProducts(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY)
    Call<List<Shop>> getShops(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY_ORDER)
    Call<List<Order>> getOrders(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY_ORDER)
    Call<Order> setOrders(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY_ORDER_BALANCE)
    Call<Order> setOrdersBalance(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY_ORDER)
    Call<Order> repeatOrder(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY_ORDER_BALANCE)
    Call<Order> repeatOrderBalance(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY_ORDER)
    Call<String> cancelOrder(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY_ORDER_BALANCE)
    Call<String> cancelOrderBalance(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @GET(AppConstants.QUERY_STATUS_OBJECT)
    Call<StatusResponse> getStatuses(@Header("Authorization") String bearerToken);

    @POST(AppConstants.QUERY)
    Call<String> sendFBtoken(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY)
    Call<List<Banner>> getBanners(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

    @POST(AppConstants.QUERY)
    Call<List<Story>> getStories(@Header("Authorization") String bearerToken, @Body CmdBody cmdBody);

}
