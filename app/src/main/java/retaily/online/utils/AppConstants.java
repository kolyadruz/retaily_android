package retaily.online.utils;

public class AppConstants {
    //public static final String SERVICE_URL ="https://yakit.pro:8088/apiretaily/";
    public static final String SERVICE_URL ="https://retaily.online/api/";
    //public static final String SERVICE_URL ="https://retaily.online/api_test/";

    public static final String AUTH = "users/auth";

    public static final String QUERY = "clientv2";

    public static final String QUERY_ORDER = QUERY + "/order";
    public static final String QUERY_ORDER_BALANCE = QUERY + "/order_balance";

    public static final String QUERY_STATUS_OBJECT = QUERY + "/objects/status";
    public static final String REPO = "repo/";

    public static final String SUPPORT_PHONE = "88005500103";//"+79243677748";

    //local
    public static final String DB_NAME = "yakitDB";
    public static final String PREF_NAME = "prefs";

    public static final String SHOW_ORDERS_ACTION = "SHOW_ORDERS";

    //ErrorMessages
    public static final String ON_TOKEN_EXPIRED = "Токен устарел. Требуется повторная авторизация";
    public static final String ON_INTERNAL_SERVER_ERROR = "Неверный логин или пароль";
    public static final String ON_SERVICE_UNAVAILABLE = "Сервис временно недоступен";
    public static final String ON_SERVER_ERROR = "Ошибка объекта сервера";
}
