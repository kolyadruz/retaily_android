package retaily.online.utils;

public class PrefKeys {

    public static final String TOKEN_KEY = "token";
    public static final String LOGIN_KEY = "login";
    public static final String PASS_KEY = "pass";
    public static final String PIN_KEY = "pin";
    public static final String CURRENT_USER_ID_KEY = "user_id";

}
