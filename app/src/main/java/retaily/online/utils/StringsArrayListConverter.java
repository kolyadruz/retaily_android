package retaily.online.utils;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class StringsArrayListConverter {

    @TypeConverter
    public static ArrayList<String> jsonToArray(String json) {
        Type listType = new TypeToken<ArrayList<String>>() {}.getType();
        return new Gson().fromJson(json, listType);
    }

    @TypeConverter
    public static String arrayToJson(ArrayList<String> arrayList) {
        Gson gson = new Gson();
        String json = gson.toJson(arrayList);
        return json;
    }

}
