package retaily.online.utils;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import retaily.online.models.response.getsuppliers.Supplier;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SuppliersListConverter {

    @TypeConverter
    public static ArrayList<Supplier> jsonToSuppliers(String json) {
        Type listType = new TypeToken<List<Supplier>>() {}.getType();
        return new Gson().fromJson(json, listType);
    }

    @TypeConverter
    public static String suppliersToJson(ArrayList<Supplier> suppliers) {
        Gson gson = new Gson();
        String json = gson.toJson(suppliers);
        return json;
    }

}
