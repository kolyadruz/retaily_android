package com.rahuljanagouda.statusstories.glideProgressBar;

import androidx.annotation.Nullable;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

/**
 * Created by rahuljanagouda on 30/09/17.
 */

public final class NoOpRequestListener<A, B> implements RequestListener<A> {
    private static final RequestListener INSTANCE = new NoOpRequestListener();

    @SuppressWarnings("unchecked")
    public static <A, B> RequestListener<A> get() {
        return INSTANCE;
    }

    private NoOpRequestListener() {
    }

    public boolean onException(Exception e, A a, Target<B> target, boolean b) {
        return false;
    }

    @Override
    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<A> target, boolean isFirstResource) {
        return false;
    }

    @Override
    public boolean onResourceReady(A resource, Object model, Target<A> target, DataSource dataSource, boolean isFirstResource) {
        return false;
    }
}
